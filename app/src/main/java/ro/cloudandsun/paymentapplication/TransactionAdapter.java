package ro.cloudandsun.paymentapplication;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.List;

/**
 * Created by LG on 20.09.2017.
 */

public class TransactionAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<SHCLOG> transactions;


    public TransactionAdapter(Activity activity, List<SHCLOG> transactions) {
        layoutInflater = (LayoutInflater) activity.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        this.transactions = transactions;

    }

    @Override
    public int getCount() {
        return transactions.size();
    }

    @Override
    public SHCLOG getItem(int position) {
        return transactions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;
        rowView = layoutInflater.inflate(R.layout.transactions_row_layout, null);
        TextView paymentType = rowView.findViewById(R.id.paymentType);
        TextView status = rowView.findViewById(R.id.status);
        TextView netAmount = rowView.findViewById(R.id.netAmount);
        TextView createdDate = rowView.findViewById(R.id.createdDate);

        SHCLOG transaction = transactions.get(position);
        int id = 0;
        if (transaction.getReceiptNumber() != null) {
            id = Integer.parseInt(transaction.getReceiptNumber());
        }
        rowView.setId(id);

        netAmount.setText(Double.toString(Double.parseDouble(transaction.getAmount())) + " LEI");
        paymentType.setText("Txn.Status: " + transaction.getTxnStatus());
        status.setText("Rec.No: " + transaction.getReceiptNumber());

        Date TxnDate = AppHelper.ConvertToDate(transaction.getCreatedDate(), "yyyy-MM-dd hh:mm:ss");
        String FormatedDate = transaction.getTxnDateTime().substring(0, 4) + "-" + transaction.getTxnDateTime().substring(4, 6) + "-" + transaction.getTxnDateTime().substring(6, 8);
        FormatedDate += " " + transaction.getTxnDateTime().substring(8, 10) + ":" + transaction.getTxnDateTime().substring(10, 12) + ":" + transaction.getTxnDateTime().substring(12, 14);

        createdDate.setText(FormatedDate);

        RelativeLayout relativeLayout = rowView.findViewById(R.id.leftSide);
        int color = Color.parseColor("#414b55");
        int RespCode = Integer.parseInt(transaction.getRespCode());


        String msgType = transactions.get(position).getMsgType();

        if(!msgType.equals(null)) {
            switch (msgType) {
                case "210":
                    //sale
                    if(RespCode == 0 ) {
                        color = Color.parseColor("#14af96"); //green_IOS sale
                    }else{
                        color=Color.parseColor("#FFF10707"); //red_IOS rejected
                    }
                    break;
                case "400":
                case "410":
                    //cancel or auto cancel
                    color = Color.parseColor("#414b55"); //black
                    break;
                default:
                    // color = RespCode == 0 ? Color.parseColor("#333") : Color.parseColor("#4577c6"); //gray L blue
                    if(RespCode != 0 ){
                        color=Color.parseColor("#FFF10707"); //rsp code not 0 is red rejected
                    }
                    break;
            }

        }else{
            Log.d("ADAPTOR","The message type for the transaction is "+msgType.toString());
        }

        relativeLayout.setBackgroundColor(color);
        status.setTextColor(color);
        netAmount.setTextColor(color);

        return rowView;
    }

}