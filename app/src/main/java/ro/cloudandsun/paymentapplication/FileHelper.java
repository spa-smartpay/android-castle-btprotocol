package ro.cloudandsun.paymentapplication;

import android.content.Context;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by LG on 15.09.2017.
 */

public class FileHelper {

    public boolean WriteToFile(Context context, String FileName, String Data) {
        try {
            BufferedWriter bufferedWriter = null;
            bufferedWriter = new BufferedWriter(new FileWriter(new File(context.getFilesDir() + File.separator + FileName)));
            bufferedWriter.write(Data);
            bufferedWriter.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String ReadFromFile(Context context, String FileName) {
        String Result = "";
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(new
                    File(context.getFilesDir() + File.separator + FileName)));
            String read;

            while ((read = bufferedReader.readLine()) != null) {
                Result += read;
            }
            bufferedReader.close();
            return Result;

        } catch (IOException e) {
            Result = "";
        }
        return Result;
    }

    public boolean fileExist(String fileName, Context context) {
        String path = context.getFilesDir().getAbsolutePath() + "/" + fileName;
        File file = new File(path);
        return file.exists();
    }

}



