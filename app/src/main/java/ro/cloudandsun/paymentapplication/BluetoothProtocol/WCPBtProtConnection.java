package ro.cloudandsun.paymentapplication.BluetoothProtocol;

/**
 * Created by Forest on 23.03.2018.
 */

public class WCPBtProtConnection {
    public String szHostIP;
    public String szHostPort;
    public int iDestType;
    public int iCAIndex;
    public int iCommType;
    public int iConnTimeout;
    public int iSendTimeout;
    public int iRecvTimeout;

    public WCPBtProtConnection(){

    }

    public WCPBtProtConnection(String a_szHostIP,
                               String a_szHostPort,
                               int a_iDestType,
                               int a_iCommType,
                               int a_iConnTimeout,
                               int a_iSendTimeout,
                               int a_iRecvTimeout)
    {
        this.szHostIP = a_szHostIP;
        this.szHostPort =a_szHostPort;
        this.iDestType =a_iDestType;
        this.iCommType =a_iCommType;
        this.iConnTimeout = a_iConnTimeout;
        this.iSendTimeout = a_iSendTimeout;
        this.iRecvTimeout = a_iRecvTimeout;
    }
}
