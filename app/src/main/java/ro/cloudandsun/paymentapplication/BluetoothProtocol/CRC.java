package ro.cloudandsun.paymentapplication.BluetoothProtocol;

/**
 * The Class CRC.
 *
 * @author MARiUS
 */
public class CRC {

    /** The Constant CRC_POLYNOM. */
    private static final int CRC_POLYNOM = (int) 0x00008005;

    /** The Constant CRC_MASK. */
    private static final int CRC_MASK = (int) 0x00008000;

    /**
     * Calculate CRC.
     *
     * @param start
     *            the start value
     *
     * @param buffer
     *            the buffer
     *
     * @param length
     *            the length of buffer
     *
     * @return the CRC value
     */
    public static int CalculateCRC(int start, byte[] buffer, int length) {
        int crc = start;

        for (int i = 0; i < length; i++) {
            crc ^= (int) ((buffer[i] << 8) & 0x0000FFFF);
            for (int j = 0; j < 8; j++) {
                if ((crc & CRC_MASK) == CRC_MASK) {
                    crc <<= 1;
                    crc = crc ^ CRC_POLYNOM;
                } else {
                    crc <<= 1;
                }

                crc &= 0x0000FFFF;
            }
        }

        return crc;
    }
}
