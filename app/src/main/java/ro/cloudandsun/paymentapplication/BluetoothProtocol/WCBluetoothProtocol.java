package ro.cloudandsun.paymentapplication.BluetoothProtocol;

import android.util.Log;

/**
 * Created by Forest on 23.03.2018.
 */

public class WCBluetoothProtocol {

    public static final byte STX = 0x02;
    public static final byte ETX = 0x03;
    public static final byte ENQ = 0x05;
    public static final byte ACK = 0x06;
    public static final byte NAK = 0x15;
    public static final byte EOT = 0x04;

    public static final int WC_MSG_DATA_MAX_LEN = 4096+128;

    public static final int g_cusWCTAG_COMMAND = 0xF000;
    public static final int g_cusWCTAG_RESPONSE = 0xF100;

    public static final byte WC_DO_OPEN_CMD = (byte) 0xF1;
    public static final byte WC_DO_CONNECT_CMD =(byte) 0xF2;
    public static final byte WC_DO_SEND_RECV_P_CMD =(byte)  0xF3;
    public static final byte WC_DO_SEND_RECV_U_CMD =(byte)  0xF4;
    public static final byte WC_DO_SEND_ONLY_CMD =(byte)  0xF5;
    public static final byte WC_DO_RECV_ONLY_CMD =(byte)  0xF6;
    public static final byte WC_DO_DISCONNECT =(byte)  0xF7;
    public static final byte WC_DO_CLOSE_CMD =(byte)  0xF8;

    public static final byte WC_DO_FINANCIAL_TRX =(byte)  0xF9;

    public static final int g_cusWCTAG_HOST_IP = 0xE001;
    public static final int g_cusWCTAG_HOST_PORT = 0xE002;
    public static final int g_cusWCTAG_HOST_DEST_TYPE = 0xE003;
    public static final int g_cusWCTAG_HOST_COMM_TYPE = 0xE004;
    public static final int g_cusWCTAG_HOST_CA_INDEX = 0xE005;
    public static final int g_cusWCTAG_HOST_CONN_TIMEOUT = 0xE006;
    public static final int g_cusWCTAG_HOST_SEND_TIMEOUT = 0xE007;
    public static final int g_cusWCTAG_HOST_RECV_TIMEOUT = 0xE008;

    public static final int g_cusWCTAG_DATA_BUFF = 0xF001;
    public static final int g_cusWCTAG_DATA_MAX_LEN = 0xF002;

    public static final byte WC_TX_TYPE_SALE = (byte) 0xA1;
    public static final byte WC_TX_TYPE_SALE_CB = (byte) 0xA2;
    public static final byte WC_TX_TYPE_VOID = (byte) 0xA3;

    public static final int g_cusWCTAG_REQ_TX_TYPE            = 0xF003;
    public static final int g_cusWCTAG_REQ_TX_AMT             = 0xF004;
    public static final int g_cusWCTAG_REQ_TX_CBAMT           = 0xF005;
    public static final int g_cusWCTAG_REQ_TX_RCPNO           = 0xF006;

    public static final int g_cusWCTAG_RESP_TX_AMT            = 0xF101;
    public static final int g_cusWCTAG_RESP_TX_CBAMT          = 0xF102;
    public static final int g_cusWCTAG_RESP_TX_RCPNO          = 0xF103;
    public static final int g_cusWCTAG_RESP_TX_RESP_CODE      = 0xF104;
    public static final int g_cusWCTAG_RESP_TX_RRN            = 0xF105;
    public static final int g_cusWCTAG_RESP_TX_AUTH_CODE      = 0xF106;


    /**
     * Process received request from mPOS
     *
     * @param wcBtProtReceive
     * @return WCBtProtResponse - response data or null if invalid request received
     */
    public WCBtProtSend ProcessRequest(WCBtProtReceive wcBtProtReceive) {
        int iCnt = 0;
        byte[] byBuff = new byte[WCBluetoothProtocol.WC_MSG_DATA_MAX_LEN];
        int iCmd = 0;
        byte byCmd = 0x00;

        WCBtProtSend protResponse = new WCBtProtSend();

        if (wcBtProtReceive.iDataBufferLen < 6) {
            Log.d("wcProtBt", "received data is less than 3");
            return null;
        } else {

            Log.d("wcProtBt", "received data is greater than 3");
            //Check if STX received
            if (wcBtProtReceive.dataBuffer[0] == STX) {

                Log.d("wcProtBt", "STX received");
                iCnt++;

                //Next 2 bytes are the total data size
                System.arraycopy(wcBtProtReceive.dataBuffer, iCnt, byBuff, 0, 2);
                Log.d("wcProtBt", "First 2 bytes : \n" + HexUtils.formatHexDump(byBuff, 0, 2));
                iCnt += 2;

                //Search the command tag & value
                byBuff = WCBtProtUtils.GetTagBuffer(g_cusWCTAG_COMMAND, wcBtProtReceive.dataBuffer, wcBtProtReceive.iDataBufferLen);
                if (byBuff == null || byBuff.length == 0) {
                    Log.d("wcProtBt", "Could not find command tag :");
                    return null;
                }

                Log.d("wcProtBt", "First 2 bytes : \n" + HexUtils.formatHexDump(byBuff, 0, 2));

                //Get command value
                byCmd = byBuff[0];

                //Search for ETX & CRC
                if (wcBtProtReceive.dataBuffer[wcBtProtReceive.dataBuffer.length - 3] != ETX) {
                    //ETX not received
                    return null;
                }

                //Extract CRC
                byte[] byCrc = new byte[2];
                System.arraycopy(wcBtProtReceive.dataBuffer, wcBtProtReceive.dataBuffer.length - 2, byCrc, 0, 2);
                byte[] byDataBody = new byte[wcBtProtReceive.dataBuffer.length - iCnt  - 3];
                System.arraycopy(wcBtProtReceive.dataBuffer, iCnt, byDataBody, 0, wcBtProtReceive.dataBuffer.length - iCnt - 3);
                int iRecvCRC = WCBtProtUtils.ByteArrToInt(byCrc);
                int iCalcCRC = CRC.CalculateCRC(0 , byDataBody , byDataBody.length);

                if (iRecvCRC != iCalcCRC) {
                    Log.d("wcProtBt", "CRC does not match calc [" + iCalcCRC + "] recv [" + iRecvCRC + "]\n");
                    return null;
                }

                switch (byCmd) {
                    case WC_DO_OPEN_CMD:
                        Log.d("wcProtBt", "Command [OPEN] received");
                        protResponse= wcBtProtReceive.ProcessOpenCommand();
                        break;
                    case WC_DO_CONNECT_CMD:
                        Log.d("wcProtBt", "Command [CONNECT] received");
                        protResponse = wcBtProtReceive.ProcessConnectCommand();
                        break;
                    case WC_DO_SEND_ONLY_CMD:
                        Log.d("wcProtBt", "Command [WC_DO_SEND_ONLY_CMD] received");
                        protResponse = wcBtProtReceive.ProcessSendCommand();
                        break;
                    case WC_DO_RECV_ONLY_CMD:
                        Log.d("wcProtBt", "Command [WC_DO_RECV_ONLY_CMD] received");
                        protResponse = wcBtProtReceive.ProcessRecvCommand();
                        break;
                    case WC_DO_SEND_RECV_P_CMD:
                        Log.d("wcProtBt", "Command [SEND/RECV P] received");
                        protResponse = wcBtProtReceive.ProcessSendRecvCommand_P();
                        break;
                    case WC_DO_SEND_RECV_U_CMD:
                        Log.d("wcProtBt", "Command [SEND/RECV U] received");
                        protResponse = wcBtProtReceive.ProcessSendRecvCommand_U();
                        break;
                    case WC_DO_DISCONNECT:
                        Log.d("wcProtBt", "Command [DISCONNECT] received");
                        protResponse = wcBtProtReceive.ProcessDisconnectCommand();
                        break;
                    case WC_DO_CLOSE_CMD:
                        Log.d("wcProtBt", "Command [CLOSE] received");
                        protResponse = wcBtProtReceive.ProcessCloseCommand();
                        break;
                    case WC_DO_FINANCIAL_TRX:
                        Log.d("wcProtBt", "Command [FINANCIAL TX] received");
                        protResponse = wcBtProtReceive.ProcessTrxResponse();
                        break;
                }

            }
        }


        return protResponse;
    }

    public boolean PackResponse(WCBtProtSend wcBtProtResponse) {
        try{
            if(wcBtProtResponse.PackResponse())
            {
                Log.d("prot" , "PackResponse OK ");
                Log.d("prot", HexUtils.formatHexDump(wcBtProtResponse.dataBuffer, 0, wcBtProtResponse.iDataBufferLen));
                return  true;
            }

            Log.d("prot" , "PackResponse NOK ");
            return false;
        }catch (Exception ex)
        {
            Log.d("prot" , "PackResponse exception : " + ex.getMessage());
            return false;
        }
    }
}
