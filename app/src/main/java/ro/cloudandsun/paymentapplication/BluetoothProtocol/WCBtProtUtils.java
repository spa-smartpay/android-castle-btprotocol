package ro.cloudandsun.paymentapplication.BluetoothProtocol;
import android.util.Log;

import java.nio.charset.StandardCharsets;

/**
 * Created by Forest on 23.03.2018.
 */

public class WCBtProtUtils {

    /**
     * Parse byte array buffer for tag and returns value
     *
     * @param a_iTag     - searched tag value
     * @param a_byBuffer - buffer to be parsed
     * @param a_iLen     - length of buffer
     * @return byte array of buffer
     */
    public static byte[] GetTagBuffer(int a_iTag, byte[] a_byBuffer, int a_iLen) {
        byte[] bytes = null;
        int iCnt = 0;
        int iLen = 0;
        byte b1 = (byte) (a_iTag / 256);
        byte b2 = (byte) (a_iTag % 256);

        Log.d("utils", "Looking for TAG [" + a_iTag + "]");
        for (int i = 0; i < a_iLen; i++) {
            if (a_byBuffer[i] == b1 && a_byBuffer[i + 1] == b2)
            //Found tag
            {
                iCnt = i + 2;
                if(iCnt < a_iLen -1) {
                    //Next 2 bytes are the length of value
                    byte[] byLen = new byte[2];
                    System.arraycopy(a_byBuffer, iCnt, byLen, 0, 2);
                    iLen = ByteArrToInt(byLen);

                    iCnt += 2;
                    //Copy data from byBuffer to response
                    bytes = new byte[iLen];
                    System.arraycopy(a_byBuffer, iCnt, bytes, 0, iLen);

                    Log.d("utils", "Found TAG [" + a_iTag + "] -> returning [array]");
                    Log.d("utils", HexUtils.formatHexDump(bytes, 0, iLen));
                }else {
                    bytes = new byte[0];
                }
                break;
            }
        }

        return bytes;
    }

    public static byte[] SetTagBuffer(int a_iTag, byte[] a_byBuffer, int a_iLen) {
        int iLen = 0;
        byte[] respBuff= new byte[a_iLen + 4];

        try{
            respBuff[iLen++] = (byte) (a_iTag / 256);
            respBuff[iLen++] = (byte) (a_iTag % 256);
            respBuff[iLen++] = (byte) (a_iLen / 256);
            respBuff[iLen++] = (byte) (a_iLen % 256);
            System.arraycopy(a_byBuffer , 0, respBuff , iLen ,  a_iLen);
            a_iLen += iLen;
            return respBuff;

        }catch (Exception ex)
        {
            Log.d("prot-utils" , "SetTagBuffer() -> exception " +ex.getMessage());
            return null;
        }
    }


    public static String GetTagString(int a_iTag, byte[] a_byBuffer, int a_iLen) {
        String result = "";
        byte[] bytes = WCBtProtUtils.GetTagBuffer(a_iTag, a_byBuffer, a_iLen);
        if (bytes != null) {
            result = new String(bytes, StandardCharsets.UTF_8);
            Log.d("utils" , "Found TAG ["+a_iTag+"] -> returning ["+result+"]");
        }
        return result;
    }

    public static int GetTagInt(int a_iTag, byte[] a_byBuffer, int a_iLen) {
        int result = -1;
        byte[] bytes = WCBtProtUtils.GetTagBuffer(a_iTag, a_byBuffer, a_iLen);
        if (bytes != null) {
            result = ByteArrToInt(bytes);
            Log.d("utils" , "Found TAG ["+a_iTag+"] -> returning ["+result+"]");
        }
        return result;
    }

    public static byte[] AddHeaders( byte[] a_byBuffer)
    {
        int iLen = 0;
        byte[] respBuff= new byte[a_byBuffer.length + 6];
        //Calculate CRC
        int sCrc = CRC.CalculateCRC(0 , a_byBuffer , a_byBuffer.length);
        //Add STX
        respBuff[iLen++] = WCBluetoothProtocol.STX;
        //Add length
        respBuff[iLen++] = (byte) (a_byBuffer.length / 256);
        respBuff[iLen++] = (byte) (a_byBuffer.length % 256);
        //Add buffer data
        System.arraycopy(a_byBuffer , 0 , respBuff , iLen ,  a_byBuffer.length);
        iLen+=a_byBuffer.length;
        //Add ETX
        respBuff[iLen++] = WCBluetoothProtocol.ETX;
        //Add CRC
        respBuff[iLen++] = (byte) (sCrc / 256);
        respBuff[iLen++] = (byte) (sCrc % 256);

        return respBuff;
    }


    public static short Calculate_CRC(byte[] bytes) {
        int i;
        int crc_value = 0;
        for (int len = 0; len < bytes.length; len++) {
            for (i = 0x80; i != 0; i >>= 1) {
                if ((crc_value & 0x8000) != 0) {
                    crc_value = (crc_value << 1) ^ 0x8005;
                } else {
                    crc_value = crc_value << 1;
                }
                if ((bytes[len] & i) != 0) {
                    crc_value ^= 0x8005;
                }
            }
        }
        return new Integer(crc_value).shortValue();
    }

    public static byte[] IntToByteArr(int iVal)
    {
        byte[] data = new byte[2];
        data[0] =(byte) ((iVal >> 8) & 0xff);
        data[1] =(byte) (iVal & 0xff);
        return data;
    }

    public static int ByteArrToInt(byte[] bytes)
    {
        if(bytes.length == 1) {
            Byte b = new Byte(bytes[0]);
            return b.intValue();

        }else if(bytes.length == 2){
            return ((bytes[0] & 0xff) << 8) | (bytes[1] & 0xff);
        }

        return -1;
    }
}
