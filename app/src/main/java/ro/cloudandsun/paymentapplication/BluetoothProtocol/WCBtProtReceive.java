package ro.cloudandsun.paymentapplication.BluetoothProtocol;
/**
 * Created by Forest on 23.03.2018.
 */

public class WCBtProtReceive {
    public byte[] dataBuffer;
    public int iDataBufferLen;


    public boolean isTrxResponse = false;
    public byte byTrxType;
    public int iSaleAmt;
    public int iSaleCbAmt;
    public int iRcpNo;
    public String sAuthCode;
    public String sRefNo;
    public String sRespCode;

    public WCBtProtReceive(byte[] a_dataBuffer , int a_iDataBufferLen)
    {
        this.dataBuffer = a_dataBuffer;
        this.iDataBufferLen = a_iDataBufferLen;
    }

    public WCBtProtSend ProcessOpenCommand() {
        WCBtProtSend response = new WCBtProtSend();
        //Set Command type
        response.byCommandType = WCBluetoothProtocol.WC_DO_OPEN_CMD;
        return response;
    }

    public WCBtProtSend ProcessConnectCommand() {
        int iVal = 0;
        String strBuffer;
        WCBtProtSend response = new WCBtProtSend();
        response.byCommandType = WCBluetoothProtocol.WC_DO_CONNECT_CMD;
        response.btProtConnection = new WCPBtProtConnection();
        //Get Host IP
        strBuffer = WCBtProtUtils.GetTagString(WCBluetoothProtocol.g_cusWCTAG_HOST_IP, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.szHostIP = strBuffer;
        }
        //Get Host Port
        strBuffer = WCBtProtUtils.GetTagString(WCBluetoothProtocol.g_cusWCTAG_HOST_PORT, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.szHostPort = strBuffer;
        }
        //Get Host Dest Type
        iVal = WCBtProtUtils.GetTagInt(WCBluetoothProtocol.g_cusWCTAG_HOST_DEST_TYPE, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.iDestType = iVal;
        }
        //Get Host Comm Type
        iVal = WCBtProtUtils.GetTagInt(WCBluetoothProtocol.g_cusWCTAG_HOST_COMM_TYPE, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.iCommType = iVal;
        }
        //Get Host Certificate of Authority Index
        iVal = WCBtProtUtils.GetTagInt(WCBluetoothProtocol.g_cusWCTAG_HOST_CA_INDEX, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.iCAIndex = iVal;
        }
        //Get Host Connecting timeout
        iVal = WCBtProtUtils.GetTagInt(WCBluetoothProtocol.g_cusWCTAG_HOST_CONN_TIMEOUT, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.iConnTimeout = iVal;
        }
        //Get Host Send timeout
        iVal = WCBtProtUtils.GetTagInt(WCBluetoothProtocol.g_cusWCTAG_HOST_SEND_TIMEOUT , this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.iSendTimeout = iVal;
        }
        //Get Host Receive timeout
        iVal = WCBtProtUtils.GetTagInt(WCBluetoothProtocol.g_cusWCTAG_HOST_RECV_TIMEOUT, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.iRecvTimeout = iVal;
        }
        return response;
    }

    public WCBtProtSend ProcessRecvCommand() {
        byte[] byBuff;
        WCBtProtSend response = new WCBtProtSend();

        response.byCommandType = WCBluetoothProtocol.WC_DO_RECV_ONLY_CMD;
        byBuff = WCBtProtUtils.GetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_DATA_MAX_LEN , this.dataBuffer, this.iDataBufferLen);
        if(byBuff != null && byBuff.length >0)
        {
            response.byCommandType = WCBluetoothProtocol.WC_DO_RECV_ONLY_CMD;
            response.dataBuffer = new byte[byBuff.length];
            response.iDataBufferLen = WCBtProtUtils.ByteArrToInt(byBuff);
            System.arraycopy(byBuff , 0 , response.dataBuffer , 0 , byBuff.length );
            iDataBufferLen = byBuff.length;
        }

        return response;
    }

    public WCBtProtSend ProcessSendCommand() {
        byte[] byBuff;
        WCBtProtSend response = new WCBtProtSend();
        response.byCommandType = WCBluetoothProtocol.WC_DO_SEND_ONLY_CMD;
        byBuff = WCBtProtUtils.GetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_DATA_BUFF , this.dataBuffer, this.iDataBufferLen);
        if(byBuff != null && byBuff.length >0)
        {
            response.byCommandType = WCBluetoothProtocol.WC_DO_SEND_ONLY_CMD;
            response.dataBuffer = new byte[byBuff.length];
            System.arraycopy(byBuff , 0 , response.dataBuffer , 0 , byBuff.length );
            response.iDataBufferLen = byBuff.length;
            iDataBufferLen = byBuff.length;
        }

        return response;
    }
    public WCBtProtSend ProcessSendRecvCommand_P() {
        byte[] byBuff;
        WCBtProtSend response = new WCBtProtSend();

        byBuff = WCBtProtUtils.GetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_DATA_BUFF , this.dataBuffer, this.iDataBufferLen);
        if(byBuff != null && byBuff.length >0)
        {
            response.byCommandType = WCBluetoothProtocol.WC_DO_SEND_RECV_P_CMD;
            response.dataBuffer = new byte[byBuff.length];
            System.arraycopy(byBuff , 0 , response.dataBuffer , 0 , byBuff.length );
            response.iDataBufferLen = byBuff.length;
            iDataBufferLen = byBuff.length;
        }

        return response;
    }

    public WCBtProtSend ProcessSendRecvCommand_U() {
        byte[] byBuff;
        WCBtProtSend response = new WCBtProtSend();

        byBuff = WCBtProtUtils.GetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_DATA_BUFF , this.dataBuffer, this.iDataBufferLen);
        if(byBuff != null && byBuff.length >0)
        {
            response.byCommandType = WCBluetoothProtocol.WC_DO_SEND_RECV_U_CMD;
            response.dataBuffer = new byte[byBuff.length];
            System.arraycopy(byBuff , 0 , response.dataBuffer , 0 , byBuff.length );
            response.iDataBufferLen = byBuff.length;
            iDataBufferLen = byBuff.length;
        }
        return response;
    }

    public WCBtProtSend ProcessDisconnectCommand() {
        WCBtProtSend response = new WCBtProtSend();
        response.byCommandType = WCBluetoothProtocol.WC_DO_DISCONNECT;
        return response;
    }

    public WCBtProtSend ProcessCloseCommand() {
        WCBtProtSend response = new WCBtProtSend();
        response.byCommandType = WCBluetoothProtocol.WC_DO_CLOSE_CMD;
        return response;
    }

    public WCBtProtSend ProcessTrxResponse() {
        int iVal = 0;
        boolean isError = false;
        String strBuffer;
        WCBtProtSend response = new WCBtProtSend();
        response.byCommandType = WCBluetoothProtocol.WC_DO_FINANCIAL_TRX;
        response.btProtConnection = new WCPBtProtConnection();

        //Get Sale Amount
        strBuffer = WCBtProtUtils.GetTagString(WCBluetoothProtocol.g_cusWCTAG_RESP_TX_AMT, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            try {
                response.iSaleAmt = Integer.parseInt(strBuffer);
            }catch (Exception ex){
                response.iSaleAmt = -1;
                isError = true;
            }
        }
        //Get Cashback Amount
        strBuffer = WCBtProtUtils.GetTagString(WCBluetoothProtocol.g_cusWCTAG_RESP_TX_CBAMT, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
           try {
               response.iSaleCbAmt = Integer.parseInt(strBuffer);
           }catch (Exception ex)
           {
               response.iSaleCbAmt = -1;
               isError = true;
           }
        }
        //Get RefNo
        strBuffer = WCBtProtUtils.GetTagString(WCBluetoothProtocol.g_cusWCTAG_RESP_TX_RRN, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
                response.sRefNo = new String(strBuffer);
        }
        //Get AuthCode
        strBuffer = WCBtProtUtils.GetTagString(WCBluetoothProtocol.g_cusWCTAG_RESP_TX_AUTH_CODE, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.sAuthCode = new String(strBuffer);
        }
        //Get Resp Code
        strBuffer = WCBtProtUtils.GetTagString(WCBluetoothProtocol.g_cusWCTAG_RESP_TX_RESP_CODE, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.sRespCode = new String(strBuffer);
            if(isError){
                response.sRespCode = "99";
            }
        }
        //Get Receipt Number
        strBuffer = WCBtProtUtils.GetTagString(WCBluetoothProtocol.g_cusWCTAG_RESP_TX_RCPNO, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            try {
                response.iRcpNo = Integer.parseInt(strBuffer);
            }catch (Exception ex)
            {
                response.iRcpNo = -1;
                isError = true;
            }
        }

        return response;
    }
}
