package ro.cloudandsun.paymentapplication.BluetoothProtocol;

import android.util.Log;


/**
 * Created by Forest on 23.03.2018.
 */

public class WCBtProtSend {
    int iResult;
    public byte byCommandType;
    public WCPBtProtConnection btProtConnection;
    public byte byTrxType;
    public int iSaleAmt;
    public int iSaleCbAmt;
    public int iRcpNo;

    public String sAuthCode;
    public String sRefNo;
    public String sRespCode;

    public byte[] dataBuffer;
    public int iDataBufferLen;

    public WCBtProtSend() {
    }

    public WCBtProtSend(byte[] a_dataBuffer, int a_iDataBufferLen) {
        this.dataBuffer = a_dataBuffer;
        this.iDataBufferLen = a_iDataBufferLen;
    }

    public boolean PackResponse() {
        boolean resp = false;

        switch (this.byCommandType) {
            case WCBluetoothProtocol.WC_DO_CONNECT_CMD:
            case WCBluetoothProtocol.WC_DO_SEND_RECV_P_CMD:
            case WCBluetoothProtocol.WC_DO_SEND_RECV_U_CMD:
            case WCBluetoothProtocol.WC_DO_RECV_ONLY_CMD:
            case WCBluetoothProtocol.WC_DO_SEND_ONLY_CMD:
                if (this.iDataBufferLen > 0) {
                    Log.d("wcProtBt - resp", "Command [" + this.byCommandType + "] received");
                    if (this.PackSendRecvCommand()) {
                        resp = true;
                    }
                } else {
                    resp = false;
                }
                break;
            case WCBluetoothProtocol.WC_DO_FINANCIAL_TRX:
                Log.d("wcProtBt - resp", "Command [" + this.byCommandType + "] received");
                if (this.PackEcrCommand()) {
                    resp = true;
                }
                break;
        }
        return resp;
    }

    private boolean PackEcrCommand() {
        byte[] respBuff = new byte[0];
        byte[] buff;
        int iTtlCnt = 0;
        String strAmt;
        String strCbAmt;
        String strRcpNo;
        byte[] fullRespBuff = new byte[WCBluetoothProtocol.WC_MSG_DATA_MAX_LEN];



        switch (this.byTrxType) {
            case WCBluetoothProtocol.WC_TX_TYPE_SALE:
                //Set Transaction type
                buff = new byte[1];
                buff[0] = byTrxType;
                respBuff = WCBtProtUtils.SetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_REQ_TX_TYPE, buff, 1);
                System.arraycopy(respBuff, 0, fullRespBuff, iTtlCnt, respBuff.length);
                iTtlCnt += respBuff.length;

                //Set Amount
                strAmt = String.format("%012d", this.iSaleAmt);
                respBuff = WCBtProtUtils.SetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_REQ_TX_AMT, strAmt.getBytes(), strAmt.getBytes().length);
                System.arraycopy(respBuff, 0, fullRespBuff, iTtlCnt, respBuff.length);
                iTtlCnt += respBuff.length;

                break;
            case WCBluetoothProtocol.WC_TX_TYPE_SALE_CB:
                //Set Transaction type
                buff = new byte[1];
                buff[0] = WCBluetoothProtocol.WC_TX_TYPE_SALE_CB;
                respBuff = WCBtProtUtils.SetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_REQ_TX_TYPE, buff, 1);
                System.arraycopy(respBuff, 0, fullRespBuff, iTtlCnt, respBuff.length);
                iTtlCnt += respBuff.length;

                //Set Amount
                strAmt = String.format("%012d", this.iSaleAmt);
                respBuff = WCBtProtUtils.SetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_REQ_TX_AMT, strAmt.getBytes(), strAmt.getBytes().length);
                System.arraycopy(respBuff, 0, fullRespBuff, iTtlCnt, respBuff.length);
                iTtlCnt += respBuff.length;

                //Set CB Amount
                strCbAmt = String.format("%012d", this.iSaleCbAmt);
                respBuff = WCBtProtUtils.SetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_REQ_TX_CBAMT, strCbAmt.getBytes(), strCbAmt.getBytes().length);
                System.arraycopy(respBuff, 0, fullRespBuff, iTtlCnt, respBuff.length);
                iTtlCnt += respBuff.length;

                break;
            case WCBluetoothProtocol.WC_TX_TYPE_VOID:
                //Set Transaction type
                buff = new byte[1];
                buff[0] = WCBluetoothProtocol.WC_TX_TYPE_VOID;
                respBuff = WCBtProtUtils.SetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_REQ_TX_TYPE, buff, 1);
                System.arraycopy(respBuff, 0, fullRespBuff, iTtlCnt, respBuff.length);
                iTtlCnt += respBuff.length;

                //Set Amount
                strAmt = String.format("%012d", this.iSaleAmt);
                respBuff = WCBtProtUtils.SetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_REQ_TX_AMT, strAmt.getBytes(), strAmt.getBytes().length);
                System.arraycopy(respBuff, 0, fullRespBuff, iTtlCnt, respBuff.length);
                iTtlCnt += respBuff.length;

                //Set Receipt number
                strRcpNo = String.format("%06d", this.iRcpNo);
                respBuff = WCBtProtUtils.SetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_REQ_TX_RCPNO, strRcpNo.getBytes(), strRcpNo.getBytes().length);
                System.arraycopy(respBuff, 0, fullRespBuff, iTtlCnt, respBuff.length);
                iTtlCnt += respBuff.length;

                break;
        }

        Log.d("wcProtBt - resp", "fullRespBuff :" + HexUtils.formatHexDump(fullRespBuff, 0, iTtlCnt));

        if(fullRespBuff.length >0 ) {
            this.dataBuffer = new byte[iTtlCnt];
            System.arraycopy(fullRespBuff, 0,  this.dataBuffer, 0, iTtlCnt);
            this.dataBuffer = WCBtProtUtils.AddHeaders(this.dataBuffer);
            this.iDataBufferLen = this.dataBuffer.length;
            if (this.dataBuffer != null && this.iDataBufferLen > 0) {
                return true;
            } else {
                return false;
            }
        }else {
            return false;
        }
    }

    private boolean PackSendRecvCommand() {

        byte[] respBuff = WCBtProtUtils.SetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_RESPONSE, this.dataBuffer, this.iDataBufferLen);
        byte[] fullRespBuff = WCBtProtUtils.AddHeaders(respBuff);
        this.dataBuffer = fullRespBuff;
        this.iDataBufferLen = fullRespBuff.length;
        if (this.dataBuffer != null && this.iDataBufferLen > 0) {
            return true;
        } else {
            return false;
        }
    }
}
