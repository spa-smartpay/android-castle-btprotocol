package ro.cloudandsun.paymentapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.bridge.Bridge;
import com.afollestad.bridge.Form;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import app.akexorcist.bluetotohspp.library.DeviceList;
import ro.cloudandsun.paymentapplication.BluetoothProtocol.WCBluetoothProtocol;
import ro.cloudandsun.paymentapplication.BluetoothProtocol.WCBtProtReceive;
import ro.cloudandsun.paymentapplication.BluetoothProtocol.WCBtProtSend;

import static java.util.Calendar.getInstance;
import static ro.cloudandsun.paymentapplication.CaptureSignatureActivity.SIGNATURE_ACTIVITY;
import static ro.cloudandsun.paymentapplication.ProvusSSL.formatHexDump;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static SHCLOG transaction = null;
    public static SettingsModel settingsModel = new SettingsModel();
    public static ModelTerminal terminalModel = new ModelTerminal();
    public static boolean isBluetoothAvailable = false;
    public static BluetoothDD bluetoothSPP;
    public static BluetoothAdapter bluetoothAdapter;
    public static String bluetoothReceivedMessage = "";
    public static int saleSteps = 0;
    public static MaterialDialog progress_dialog;
    public static boolean progress_dialog_launched = false;
    public static ViewGroup mainLayout;
    public static LayoutInflater inflater;
    public static Intent DeviceListIntent;
    public static boolean langIsChanged = false;
    private static Context context;
    private static InputStream certInStream;

    private final BroadcastReceiver BluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        bluetoothSPP.stopService();
                        //Toast.makeText(getApplicationContext(), "Turning Bluetooth off...", Toast.LENGTH_SHORT).show();
                        break;
                    case BluetoothAdapter.STATE_ON:
                        //Toast.makeText(getApplicationContext(), "Bluetooth is on.", Toast.LENGTH_SHORT).show();
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        break;
                }
            }else if(action.equals(BluetoothDevice.ACTION_ACL_DISCONNECTED)){
                bluetoothSPP.stopService();
            }
        }
    };

    public boolean savedSettings = false;
    public boolean btInit = false;
    ArrayList<SHCLOG> transactions;
    private Boolean exit = false;
    private ContinuousTcpClient continuousTcpClient;

    public static Context getAppContext() {
        return context;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onRestart() {
        super.onRestart();

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null) {
            if (!bluetoothAdapter.isEnabled()) {
                bluetoothAdapter.enable();
                bluetoothAdapter.cancelDiscovery();
                bluetoothAdapter.startDiscovery();
                Toast.makeText(this, getString(R.string.BluetoothIsEnabled), Toast.LENGTH_SHORT).show();
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case SIGNATURE_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status = bundle.getString("status");
                    if (status.equalsIgnoreCase("done")) {
                        new MaterialDialog
                                .Builder(this)
                                .title(getString(R.string.sale) + " " + getString(R.string.succeed))
                                .iconRes(R.drawable.okay_flat_colored)
                                .limitIconToDefaultSize()
                                .positiveColorRes(R.color.red_IOS)
                                .positiveText(getString(R.string.OK))
                                .show();
                    }
                }
                break;
        }

        if (requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {

            progress_dialog.create();
            progress_dialog.show();
            timerDelayRemoveDialog(15000, progress_dialog);
            progress_dialog_launched = true;

            if (resultCode == Activity.RESULT_OK) {
                bluetoothSPP.connect(data);
            } else {
                if (progress_dialog_launched) {
                    progress_dialog.cancel();
                    progress_dialog_launched = false;
                }
            }

        } else if (requestCode == BluetoothState.REQUEST_ENABLE_BT) {

            if (resultCode == Activity.RESULT_OK) {
                bluetoothSPP.setupService();
                bluetoothSPP.startService(BluetoothState.DEVICE_OTHER);
            } else {
                // no selected any device
                if (progress_dialog_launched) {
                    progress_dialog.cancel();
                    progress_dialog_launched = false;
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        mainLayout = (LinearLayout) findViewById(R.id.main_container);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mainLayout.getChildAt(0).setTag("main");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        bluetoothInit();

        try {
            certInStream = getAssets().open("provus.bks");
            ;
        } catch (Exception ex) {
            Log.e("mainActivity", "Cert input stream error" + ex);
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(BluetoothReceiver, filter);

// BADGET
        String className = null;
        PackageManager pm = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                className = resolveInfo.activityInfo.name;
            }
        }
        intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        // intent.putExtra("badge_count", 10);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", className);
        context.sendBroadcast(intent);
// END BADGET

        Gson gson = new Gson();
        String terminalData = (new FileHelper()).ReadFromFile(this, "terminal.data");
        MainActivity.terminalModel = (new Gson()).fromJson(terminalData, ModelTerminal.class);
        //Toast.makeText(this, MainActivity.terminalModel.SNO, Toast.LENGTH_LONG).show();

        MainActivity.settingsModel.GetNotification = false;
        MainActivity.settingsModel.ShowSignature = false;
        MainActivity.settingsModel.LogoffWhenExit = true;
        MainActivity.settingsModel.ExitConfirmation = false;
        MainActivity.settingsModel.Language = "ro";

        if ((new FileHelper()).fileExist("settings.data", MainActivity.this)) {
            MainActivity.settingsModel = gson.fromJson((new FileHelper()).ReadFromFile(this, "settings.data"), SettingsModel.class);
        } else {
            (new FileHelper()).WriteToFile(this, "settings.data", gson.toJson(MainActivity.settingsModel));
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        DeviceListIntent = new Intent(this, DeviceList.class);
        DeviceListIntent.putExtra("scan_for_devices", "Paired Devices");

        progress_dialog = new MaterialDialog.Builder(context)
                .progress(true, 0)
                .cancelable(false)
                .content(R.string.please_wait)
                .autoDismiss(true)
                .build();

    }

    public int readShort(byte[] data, int offset) {
        int ch1 = data[offset] & 0xff;
        int ch2 = data[offset + 1] & 0xff;
        return (ch1 << 8) + (ch2 << 0);
    }

    public void bluetoothInit() {

        if (FirstScreen.btIsEnabled) {
            Toast.makeText(this, getString(R.string.BluetoothIsEnabled), Toast.LENGTH_SHORT).show();
            FirstScreen.btIsEnabled = false;
        }

        bluetoothSPP = new BluetoothDD(this);
        if (bluetoothSPP.isBluetoothAvailable()) {
            if (!bluetoothSPP.isBluetoothEnabled()) {
                bluetoothSPP.enable();
                Toast.makeText(this, getString(R.string.BluetoothIsEnabled), Toast.LENGTH_LONG).show();
            }

            if (bluetoothSPP.isBluetoothEnabled()) {
                bluetoothSPP.cancelDiscovery();
                bluetoothSPP.startDiscovery();
                bluetoothSPP.setupService();
                bluetoothSPP.startService(false);
            }

            bluetoothSPP.setBluetoothStateListener(new BluetoothDD.BluetoothStateListener() {
                public void onServiceStateChanged(int state) {
                    if (state == BluetoothState.STATE_CONNECTED) {
                        ((ImageView) findViewById(R.id.header_bluetooth_icon)).setImageResource(R.drawable.bluetooth_white);
                        if (progress_dialog_launched) {
                            progress_dialog.cancel();
                            progress_dialog_launched = false;
                        }

                    } else if (state == BluetoothState.STATE_CONNECTING) {
                        ((ImageView) findViewById(R.id.header_bluetooth_icon)).setImageResource(R.drawable.bluetooth_black);
                    } else if (state == BluetoothState.STATE_LISTEN) {
                        ((ImageView) findViewById(R.id.header_bluetooth_icon)).setImageResource(R.drawable.bluetooth_black);
                    } else if (state == BluetoothState.STATE_NONE) {
                        ((ImageView) findViewById(R.id.header_bluetooth_icon)).setImageResource(R.drawable.bluetooth_black);
                    }else if (state == BluetoothState.STATE_DISCONNECTED) {
                        ((ImageView) findViewById(R.id.header_bluetooth_icon)).setImageResource(R.drawable.bluetooth_black);
                    }
                }
            });

            bluetoothSPP.setBluetoothConnectionListener(new BluetoothDD.BluetoothConnectionListener() {
                public void onDeviceConnected(String name, String address) {
                    Toast.makeText(MainActivity.this, R.string.conectedtodevice + "\n" + name + "\n" + address, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(MainActivity.this, Integer.toString(bluetoothSPP.getServiceState()), Toast.LENGTH_SHORT).show();
                    if (progress_dialog_launched) {
                        progress_dialog.cancel();
                        progress_dialog_launched = false;
                    }
                }

                public void onDeviceDisconnected() {
                    Toast.makeText(MainActivity.this, getString(R.string.DeviceDisconnected), Toast.LENGTH_SHORT).show();
                    ((ImageView) findViewById(R.id.header_bluetooth_icon)).setImageResource(R.drawable.bluetooth_black);
                    if (progress_dialog_launched) {
                        progress_dialog.cancel();
                        progress_dialog_launched = false;
                    }
                }

                public void onDeviceConnectionFailed() {
                    ((ImageView) findViewById(R.id.header_bluetooth_icon)).setImageResource(R.drawable.bluetooth_black);
                    Toast.makeText(MainActivity.this, getString(R.string.UnableToConnect), Toast.LENGTH_SHORT).show();
                    if (progress_dialog_launched) {
                        progress_dialog.cancel();
                        progress_dialog_launched = false;
                    }
                }
            });

            bluetoothSPP.setAutoConnectionListener(new BluetoothDD.AutoConnectionListener() {
                public void onNewConnection(String name, String address) {
                }

                public void onAutoConnectionStarted() {
                }
            });

            bluetoothSPP.setOnDataReceivedListener(new BluetoothDD.OnDataReceivedListener() {
                public void onDataReceived(final byte[] data, final String message) {


                    int DatLen = readShort(data, 0);

                    DatLen = DatLen + 2;    //add len first 2 bytes
                    String requestFromBloetouth = formatHexDump(data, 0, DatLen);

                    Log.d("TLS DATA", requestFromBloetouth);

                    bluetoothReceivedMessage = message;

                    if (!message.contains("MOL")) {
                        Log.d("TLS MOL", "NO MOL");

                        new AsyncTask() {
                            @Override
                            protected Object doInBackground(Object[] params) {
                                try {
                                    Log.d("TLS MOL", "SSL Connection");

                                    //String MSG = "00f9600017001702003038058020c0120600000000000000120000159908032303270051170700365487000019001362d18042010000063200003037393434313333373330303131383030353030323534395b850ab961cf3601405f2a020946820238008407a00000000410105f340100950500000480009a031603279c01009f02060000000012009f03060000000000009f090200029f10120110a08003220000400f00000000000000ff9f1a0206429f1e0830373934343133339f260845ce4cc0c99508019f2701809f3303e0f0c89f34034203009f3501229f3602013d9f3704f7fec3d80006323333393838001000083337303030303931";

                                    ProvusSSL provusSSL = new ProvusSSL();
                                    InputStream is = getAssets().open("provus.bks");
                                    String respMessage = provusSSL.SSLConnection(is, "X509", message, data);

                                    Log.d("TLS", "Received TLS HOST message: " + respMessage);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }
                        }.execute(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        //Log.d("TLS MOL", "MOLL FOUND");
                    }

                }
            });
        }
    }

    public static InputStream getCertInStream() {
        return certInStream;
    }

    public void onDestroy() {
        super.onDestroy();

        //continuousTcpClient.disconnect();

        if (bluetoothSPP.isBluetoothAvailable())
            bluetoothSPP.stopService();

        if (!MainActivity.settingsModel.LogoffWhenExit && savedSettings == false) {
            (new FileHelper()).WriteToFile(this, "terminal.data", "");
        }
        savedSettings = false;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawers();
        }

        if (langIsChanged) {
            langIsChanged = false;
            bluetoothSPP.stopService();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

        String v = "";
        if (mainLayout.getChildAt(0).getTag() != null) {
            v = mainLayout.getChildAt(0).getTag().toString();
        }

        if (!MainActivity.settingsModel.ExitConfirmation) {
            exit = true;
        }

        exit = true;
        if (exit && v.equals("main")) {
            MaterialDialog materialDialog = new MaterialDialog.Builder(MainActivity.this)
                    .titleColorRes(R.color.red_IOS)
                    .limitIconToDefaultSize()
                    .content(getString(R.string.AreYouSureToExit))
                    .positiveText(R.string.Yes)
                    .positiveColorRes(R.color.green_IOS)
                    .neutralText(R.string.No)
                    .neutralColorRes(R.color.red)
                    .onAny(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            if (which.name().equals("POSITIVE")) {
                                finish();
                            }
                        }
                    })
                    .show();
        } else {
            getSupportActionBar().setTitle("");
            View layout = inflater.inflate(R.layout.content_main, mainLayout, false);
            layout.setTag("main");
            mainLayout.removeAllViews();
            mainLayout.addView(layout);
            exit = true;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        final ImageView imageView = findViewById(R.id.navMenuImage);
        try {
            ((TextView) findViewById(R.id.navDeviceSNO)).setText(getString(R.string.Terminal) + " : " + MainActivity.terminalModel.SNO);
            ((TextView) findViewById(R.id.navDeviceName)).setText(getString(R.string.device) + ": " + MainActivity.terminalModel.NAME);
        } catch (Exception e) {
            //Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        LoadContent(id);
        return true;
    }

    public void OpenNavigationMenu(View v) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.openDrawer(Gravity.END);
    }

    public void CloseNavigationMenu(View v) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawers();
    }

    public void LoadContentButtonEvent(View v) {
        LoadContent(v.getId());
    }

    public void LoadContent(int id) {
        if (R.id.dashboardSaleButton == id) {
            try {
                getSupportActionBar().setTitle(getString(R.string.sale));

                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = inflater.inflate(R.layout.content_sale, mainLayout, false);
                layout.setTag("sale");
                mainLayout.removeAllViews();
                mainLayout.addView(layout);
            } catch (Exception ex) {
                //Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.saleWithCashButton) {
            try {
                getSupportActionBar().setTitle(R.string.sale_with_cash);
                View layout = inflater.inflate(R.layout.content_sale_with_cash, mainLayout, false);
                layout.setTag("salewithcash");
                mainLayout.removeAllViews();
                mainLayout.addView(layout);
            } catch (Exception ex) {
                //Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.dashboardVoidButton) {
            try {
                getSupportActionBar().setTitle(R.string.Void);
                View layout = inflater.inflate(R.layout.content_void, mainLayout, false);
                layout.setTag("void");
                mainLayout.removeAllViews();
                mainLayout.addView(layout);
                TextView editText = findViewById(R.id.totalAmount);
            } catch (Exception ex) {
                //Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.dashboardTransactionsButton) {
            if (AppHelper.isNetworkAvailable(this)) {

                final ProgressDialog pDialog;
                pDialog = new ProgressDialog(MainActivity.this);
                pDialog.setMessage(getString(R.string.please_wait));
                pDialog.setIndeterminate(true);
                pDialog.setCancelable(false);
                pDialog.create();
                pDialog.show();
                new Thread() {
                    public void run() {
                        try {
                            MainActivity.this.runOnUiThread(new Runnable() {

                                String BeginDateTrans = new SimpleDateFormat("yyyyMMdd").format(new Date()) + "000001";
                                String EndDateTrans = new SimpleDateFormat("yyyyMMdd").format(new Date()) + "235959";

                                Form form = new Form()
                                        .add("MID", MainActivity.terminalModel.SALEPOINTREFID)
                                        .add("TID", MainActivity.terminalModel.REFID)
                                        .add("StartDateTime", BeginDateTrans)
                                        .add("EndDateTime", EndDateTrans)
                                        .add("token", MainActivity.terminalModel.TOKEN);


                                final String JsonData = Bridge
                                        .post("https://posmobil.vodafone.ro/api/transactions")
                                        .body(form)
                                        .request().response().asString();

                                String StartDate = getInstance().get(Calendar.YEAR) + "" + getInstance().get(Calendar.MONTH) + "" + getInstance().get(Calendar.DAY_OF_MONTH) + "000001";
                                String EndDate = getInstance().get(Calendar.YEAR) + "" + getInstance().get(Calendar.MONTH) + "" + getInstance().get(Calendar.DAY_OF_MONTH) + "235959";

                                public void run() {
                                    pDialog.cancel();
                                    if (JsonData.isEmpty())
                                        Toast.makeText(MainActivity.this, getString(R.string.Error), Toast.LENGTH_SHORT).show();
                                    else
                                        GetTransactions(JsonData);
                                }
                            });
                        } catch (Exception e) {
                            pDialog.cancel();
                            e.printStackTrace();
                        }
                    }
                }.start();


            } else {
                Toast.makeText(this, getString(R.string.Nointernetconnection), Toast.LENGTH_LONG).show();
            }
        } else if (id == R.id.settingsButton) {
            SettingButtonAction();
        } else if (id == R.id.logout) {

            MaterialDialog materialDialog = new MaterialDialog.Builder(MainActivity.this)
                    .titleColorRes(R.color.red_IOS)
                    .limitIconToDefaultSize()
                    .content(getString(R.string.AreYouSureToExit))
                    .positiveText(R.string.Yes)
                    .positiveColorRes(R.color.green_IOS)
                    .neutralText(R.string.No)
                    .neutralColorRes(R.color.red)
                    .onAny(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            if (which.name().equals("POSITIVE")) {
                                finish();
                                LogOut();
                            }
                        }
                    })
                    .show();

        } else if (id == R.id.notifications) {
            new AlertDialog
                    .Builder(this, R.style.AppTheme_FullScreen)
                    .setTitle(getString(R.string.notifications_head))
                    .setMessage(getString(R.string.notifications_body))
                    .setCancelable(true)
                    .show();
        } else if (id == R.id.qaPage) {
            Intent qaIntent = new Intent(getApplicationContext(), QuestionAnswersActivity.class);
            this.startActivity(qaIntent);
        } else if (id == R.id.termsConditions) {

            Intent tcIntent = new Intent(getApplicationContext(), TermsAndConditionsActivity.class);
            this.startActivity(tcIntent);

        } else if (id == R.id.policy) {

            Intent policyIntent = new Intent(getApplicationContext(), ConfidentialityPolicyActivity.class);
            this.startActivity(policyIntent);

        } else if (id == R.id.fastDial) {
            try {
                String phone_number = "+40372027777";
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone_number));
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.END);
    }

    public ArrayList<SHCLOG> filteredTransactions = null;

    public void FilterTransactions(String text, String TargetListView) {
        filteredTransactions = new ArrayList<SHCLOG>();
        for (SHCLOG t : transactions) {
            if (t.getRRN().startsWith(text) || t.getReceiptNumber().startsWith(text)) {
                filteredTransactions.add(t);
            }
        }

        final ListView listView;
        if (TargetListView.equals("transaction"))
            listView = findViewById(R.id.transactionsListView);
        else
            listView = findViewById(R.id.settlementListView);

        TransactionAdapter transactionAdapter = new TransactionAdapter(this, filteredTransactions);
        listView.setAdapter(transactionAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                transaction = filteredTransactions.get(position);

                try {
                    if (AppHelper.isNetworkAvailable(MainActivity.this)) {
                        final ProgressDialog pDialog;
                        pDialog = new ProgressDialog(MainActivity.this);
                        pDialog.setMessage(getString(R.string.please_wait));
                        pDialog.setIndeterminate(true);
                        pDialog.setCancelable(false);
                        pDialog.create();
                        pDialog.show();
                        new Thread() {
                            public void run() {
                                MainActivity.this.runOnUiThread(new Runnable() {

                                    final String ticketContent = AppHelper.createTicket(transaction);

                                    public void run() {
                                        pDialog.cancel();
                                        if (ticketContent.isEmpty())
                                            Toast.makeText(MainActivity.this, getString(R.string.Error), Toast.LENGTH_SHORT).show();
                                        else
                                            ShowTicketDetails(getString(R.string.Ticket), ticketContent);
                                    }
                                });
                            }
                        }.start();
                    } else {
                        Toast.makeText(MainActivity.this, getString(R.string.Nointernetconnection), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception ex) {
                    //Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void GetTransactions(String JsonData) {
        try {
            getSupportActionBar().setTitle(R.string.sale_reports);
            View view = inflater.inflate(R.layout.content_transactions, mainLayout, false);
            view.setTag("transactions");
            mainLayout.removeAllViews();
            mainLayout.addView(view);

            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<SHCLOG>>() {
            }.getType();

            transactions = new Gson().fromJson(JsonData, listType);

            final ListView listView = findViewById(R.id.transactionsListView);
            TransactionAdapter transactionAdapter = new TransactionAdapter(this, transactions);
            listView.setAdapter(transactionAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        transaction = transactions.get(position);
                        if (AppHelper.isNetworkAvailable(MainActivity.this)) {
                            final ProgressDialog pDialog;
                            pDialog = new ProgressDialog(MainActivity.this);
                            pDialog.setMessage(getString(R.string.please_wait));
                            pDialog.setIndeterminate(true);
                            pDialog.setCancelable(false);
                            pDialog.create();
                            pDialog.show();
                            new Thread() {
                                public void run() {
                                    MainActivity.this.runOnUiThread(new Runnable() {

                                        final String ticketContent = AppHelper.createTicket(transaction);

                                        public void run() {
                                            pDialog.cancel();
                                            if (ticketContent.isEmpty())
                                                Toast.makeText(MainActivity.this, getString(R.string.Error), Toast.LENGTH_SHORT).show();
                                            else
                                                ShowTicketDetails(getString(R.string.Ticket), ticketContent);
                                        }
                                    });
                                }
                            }.start();
                        } else {
                            Toast.makeText(MainActivity.this, getString(R.string.Nointernetconnection), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception ex) {
                        //Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            final SearchView sv = findViewById(R.id.transFilter);

            sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String text) {
                    // TODO Auto-generated method stub
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String text) {
                    FilterTransactions(text, "transaction");
                    return false;
                }
            });


        } catch (Exception ex) {
            //Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }


    public void ShowTicketDetails(String title, final String content) {
        MaterialDialog materialDialog = new MaterialDialog.Builder(MainActivity.this)
                .title(title)
                .titleColorRes(R.color.red_IOS)
                .limitIconToDefaultSize()
                .content(Html.fromHtml(content))
                .positiveText(R.string.OK)
                .positiveColorRes(R.color.green_IOS)
                .neutralText(R.string.share)
                .neutralColorRes(R.color.red)
                .onAny(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (which.name().equals("NEGATIVE")) {

                            MaterialDialog materialDialog = new MaterialDialog.Builder(MainActivity.this)
                                    .customView(R.layout.content_ticket, false)
                                    .positiveText(R.string.OK)
                                    .show();

                            View customView = materialDialog.getCustomView();
                            ((TextView) customView.findViewById(R.id.ticketTextView)).setText(content);

                        } else if (which.name().equals("NEUTRAL")) {
                            Intent emailIntent = new Intent();
                            emailIntent.setAction(Intent.ACTION_SEND);
                            emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(content).toString());
                            emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.Ticket));
                            emailIntent.setType("message/rfc822");

                            PackageManager pm = getPackageManager();
                            Intent sendIntent = new Intent(Intent.ACTION_SEND);
                            sendIntent.setType("text/plain");

                            Intent openInChooser = Intent.createChooser(emailIntent, getString(R.string.share_via));

                            List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
                            List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();

                            for (int i = 0; i < resInfo.size(); i++) {
                                ResolveInfo ri = resInfo.get(i);
                                String packageName = ri.activityInfo.packageName;
                                if (packageName.contains("email")) {
                                    emailIntent.setPackage(packageName);
                                } else if (packageName.contains("sms") || packageName.contains("messaging") || packageName.contains("mms-sms") || packageName.contains("mms") || packageName.contains("android.gm")) {
                                    Intent intent = new Intent();
                                    intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                                    intent.setAction(Intent.ACTION_SEND);
                                    intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(content).toString());
                                    intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.Ticket));
                                    intent.setType("vnd.android-dir/mms-sms");
                                    if (packageName.contains("android.gm")) { // If Gmail shows up twice, try removing this else-if clause and the reference to "android.gm" above
                                        intent.setType("message/rfc822");
                                    }

                                    intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
                                }
                            }

                            LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);

                            openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
                            startActivity(openInChooser);

                        }
                    }
                })
                .show();
    }

    private void LogOut() {

        try {
            (new FileHelper()).WriteToFile(this, "terminal.data", "");
        } catch (Exception ex) {
            Log.e("Error", ex.getMessage());
        }

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void keyButtonEvent(View v) {
        Button b = (Button) v;
        String buttonText = b.getText().toString();
        TextView totalAmount = findViewById(R.id.totalAmount);
        if (buttonText.equals("OK")) {
            buttonText = v.getTag().toString();
        }
        String strAmount = String.format("%.2f", new BigDecimal(NumericKeyFunction(buttonText, v).replace(",", ".")));
        totalAmount.setText(strAmount);

    }

    private String NumericKeyFunction(String PressedKey, View v) {
        TextView totalAmount = findViewById(R.id.totalAmount);
        String TotalHolderText = totalAmount.getText().toString();

        Button b = (Button) v;

        if (PressedKey.equals("DEL")) {
            TotalHolderText = TotalHolderText.replace(".", "").replace(",", "");

            try {
                TotalHolderText = TotalHolderText.substring(0, TotalHolderText.length() - 1);
                TotalHolderText = new StringBuilder(TotalHolderText).insert(TotalHolderText.length() - 2, ",").toString();
                if (TotalHolderText.substring(0, 1) == ",") {
                    TotalHolderText = "0" + TotalHolderText;
                }
            } catch (Exception ex) {
                //Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_SHORT).show();
            }
        } else if (PressedKey.equals("VOID")) {
            VoidTicket();
        } else if (PressedKey.equals("REFUND")) {
            RefundTicket();
        } else if (PressedKey.equals("SALE")) {
            boolean IsSaleSucceed = false;
            Double NetAmount = Double.parseDouble(TotalHolderText.replace(",", "."));
            if (NetAmount > 0) {
                if (MainActivity.settingsModel.ShowSignature) {
                    Intent intentt = new Intent(MainActivity.this, CaptureSignature.class);
                    startActivityForResult(intentt, SIGNATURE_ACTIVITY);
                }
                if (bluetoothSPP.isBluetoothAvailable()) {
                    if (bluetoothSPP.getServiceState() == BluetoothState.STATE_CONNECTED) {

                        if (progress_dialog_launched) {
                            progress_dialog.cancel();
                            progress_dialog_launched = false;
                        }

                        String TotalAmount = TotalHolderText.replace(".", ",");
                        String[] amountSplited = TotalAmount.split(",");
                        String amountLeft = amountSplited[0];
                        String amountRight = amountSplited[1];

                        if (amountRight.length() < 02)
                            amountRight += "0";

                        amountLeft = "0000000000".substring(amountLeft.length()) + amountLeft;

                        AsyncTaskForPOSMessages AsyncMsg = new AsyncTaskForPOSMessages();
                        AsyncMsg.Amount = Double.parseDouble(TotalAmount.replace(",", "."));
                        AsyncMsg.StartMessage = getString(R.string.SaleStarting);
                        AsyncMsg.EndMessage = getString(R.string.SaleStarted);
                        //Added by Sorin Padure
                        AsyncMsg.ecrCmdRequest = new WCBtProtSend();
                        AsyncMsg.ecrCmdRequest.byTrxType = WCBluetoothProtocol.WC_TX_TYPE_SALE;
                        AsyncMsg.ecrCmdRequest.iSaleAmt = Integer.parseInt(TotalAmount.replace(".", "").replace(",", ""));
                        AsyncMsg.ecrCmdRequest.iSaleCbAmt = 0;
                        AsyncMsg.ecrCmdRequest.iRcpNo = 0;
                        AsyncMsg.ecrCmdRequest.byCommandType = WCBluetoothProtocol.WC_DO_FINANCIAL_TRX;

                        AsyncMsg.execute();

                        //Toast.makeText(this, bluetoothReceivedMessage, Toast.LENGTH_SHORT).show();


                        IsSaleSucceed = true;
                        saleSteps = 1;
                    } else {
                        new MaterialDialog.Builder(this)
                                .theme(Theme.LIGHT)
                                .title(getString(R.string.Warning))
                                .content(getString(R.string.NodeviceconnectedPleaseselectadeviceandretryafterconnected))
                                .limitIconToDefaultSize()
                                .iconRes(R.drawable.cancel_asdsad)
                                .limitIconToDefaultSize()
                                .cancelable(false)
                                .positiveText(R.string.OK)
                                .onAny(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        if (which.name().equals("POSITIVE")) {
                                            if (bluetoothSPP.isBluetoothAvailable()) {
                                                startActivityForResult(DeviceListIntent, BluetoothState.REQUEST_CONNECT_DEVICE);
                                            }
                                        }
                                    }
                                })
                                .show();
                    }
                }
                if (IsSaleSucceed) {
                    //AppHelper.materialDialogCustom(MainActivity.getAppContext(),"Sale succeed", "", R.drawable.okay_05_2191542, 3000, "OKAY");
                }

            } else {
                MaterialDialog dialog = new MaterialDialog.Builder(this)
                        .theme(Theme.LIGHT)
                        .title(getString(R.string.minAmountMustBiggerZero))
                        .limitIconToDefaultSize()
                        .iconRes(R.drawable.cancel_asdsad)
                        .positiveText(R.string.OK)
                        .show();
                dialog.closeOptionsMenu();
                //  dialog.dismiss();

            }
        } else if (PressedKey.equals("CASH")) {


            boolean IsSaleSucceed = false;
            Double NetAmount = Double.parseDouble(TotalHolderText.replace(",", "."));
            if (NetAmount > 0) {
                if (MainActivity.settingsModel.ShowSignature) {
                    Intent intentt = new Intent(MainActivity.this, CaptureSignature.class);
                    startActivityForResult(intentt, SIGNATURE_ACTIVITY);
                }
                if (bluetoothSPP.isBluetoothAvailable()) {
                    if (bluetoothSPP.getServiceState() == BluetoothState.STATE_CONNECTED) {
                        if (progress_dialog_launched) {
                            progress_dialog.cancel();
                            progress_dialog_launched = false;
                        }

                        String TotalAmount = TotalHolderText.replace(".", ",");
                        String[] amountSplited = TotalAmount.split(",");
                        String amountLeft = amountSplited[0];
                        String amountRight = amountSplited[1];

                        if (amountRight.length() < 02)
                            amountRight += "0";

                        amountLeft = "0000000000".substring(amountLeft.length()) + amountLeft;

                        AsyncTaskForPOSMessages AsyncMsg = new AsyncTaskForPOSMessages();
                        AsyncMsg.Amount = Double.parseDouble(TotalAmount.replace(",", "."));
                        AsyncMsg.StartMessage = getString(R.string.CashSaleStarting);
                        AsyncMsg.EndMessage = getString(R.string.CashSaleStarted);
                        AsyncMsg.StartMessage = getString(R.string.CashSaleStarting);
                        AsyncMsg.EndMessage = getString(R.string.CashSaleStarted);

                        AsyncMsg.StartMessage = getString(R.string.CashSaleStarting);
                        AsyncMsg.EndMessage = getString(R.string.CashSaleStarted);
                        AsyncMsg.execute();

                        IsSaleSucceed = true;
                        saleSteps = 1;
                    } else {
                        new MaterialDialog.Builder(this)
                                .theme(Theme.LIGHT)
                                .title(R.string.Warning)
                                .content(R.string.NodeviceconnectedPleaseselectadeviceandretryafterconnected)
                                .limitIconToDefaultSize()
                                .iconRes(R.drawable.cancel_asdsad)
                                .limitIconToDefaultSize()
                                .cancelable(false)
                                .positiveText(R.string.OK)
                                .onAny(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        if (which.name().equals("POSITIVE")) {
                                            if (bluetoothSPP.isBluetoothAvailable()) {
                                                Intent intent = new Intent(getApplicationContext(), DeviceList.class);
                                                startActivityForResult(DeviceListIntent, BluetoothState.REQUEST_CONNECT_DEVICE);
                                            }
                                        }
                                    }
                                })
                                .show();
                    }
                }
                if (IsSaleSucceed) {
                    //AppHelper.materialDialogCustom(MainActivity.getAppContext(),"Sale succee", "", R.drawable.okay_05_2191542, 3000, "OKAY");
                }

            } else {
                new MaterialDialog.Builder(this)
                        .theme(Theme.LIGHT)
                        .title(getString(R.string.minAmountMustBiggerZero))
                        .limitIconToDefaultSize()
                        .iconRes(R.drawable.cancel_asdsad)
                        .negativeText(R.string.OK)
                        .show();
            }

        } else if (PressedKey.equals("MCSALE")) {

            boolean IsSaleSucceed = false;
            Double NetAmount = Double.parseDouble(TotalHolderText.replace(",", "."));
            if (NetAmount > 0) {
                if (MainActivity.settingsModel.ShowSignature) {
                    Intent intentt = new Intent(MainActivity.this, CaptureSignature.class);
                    startActivityForResult(intentt, SIGNATURE_ACTIVITY);
                }
                if (bluetoothSPP.isBluetoothAvailable()) {
                    if (bluetoothSPP.getServiceState() == BluetoothState.STATE_CONNECTED) {
                        if (progress_dialog_launched) {
                            progress_dialog.cancel();
                            progress_dialog_launched = false;
                        }

                        String TotalAmount = TotalHolderText.replace(".", ",");
                        String[] amountSplited = TotalAmount.split(",");
                        String amountLeft = amountSplited[0];
                        String amountRight = amountSplited[1];

                        if (amountRight.length() < 02)
                            amountRight += "0";

                        amountLeft = "0000000000".substring(amountLeft.length()) + amountLeft;

                        AsyncTaskForPOSMessages AsyncMsg = new AsyncTaskForPOSMessages();
                        AsyncMsg.Amount = Double.parseDouble(TotalAmount.replace(",", "."));
                        AsyncMsg.StartMessage = getString(R.string.MCSaleStarting);
                        AsyncMsg.EndMessage = getString(R.string.MCSaleStarted);

                        AsyncMsg.execute();

                        //Toast.makeText(this, bluetoothReceivedMessage, Toast.LENGTH_SHORT).show();

                        IsSaleSucceed = true;
                        saleSteps = 1;
                    } else {
                        new MaterialDialog.Builder(this)
                                .theme(Theme.LIGHT)
                                .title(R.string.Warning)
                                .content(R.string.NodeviceconnectedPleaseselectadeviceandretryafterconnected)
                                .limitIconToDefaultSize()
                                .iconRes(R.drawable.cancel_asdsad)
                                .limitIconToDefaultSize()
                                .cancelable(false)
                                .positiveText(R.string.OK)
                                .onAny(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        if (which.name().equals("POSITIVE")) {
                                            if (bluetoothSPP.isBluetoothAvailable()) {
                                                Intent intent = new Intent(getApplicationContext(), DeviceList.class);
                                                startActivityForResult(DeviceListIntent, BluetoothState.REQUEST_CONNECT_DEVICE);
                                            }
                                        }
                                    }
                                })
                                .show();
                    }
                }
                if (IsSaleSucceed) {
                    //AppHelper.materialDialogCustom(MainActivity.getAppContext(),"Sale succeed", "", R.drawable.okay_05_2191542, 3000, "OKAY");
                }

            } else {
                new MaterialDialog.Builder(this)
                        .theme(Theme.LIGHT)
                        .title(getString(R.string.minAmountMustBiggerZero))
                        .limitIconToDefaultSize()
                        .iconRes(R.drawable.cancel_asdsad)
                        .positiveText(R.string.OK)
                        .show();
            }


        } else {
            if (TotalHolderText.length() < 12) {
                TotalHolderText = TotalHolderText.replace(".", "").replace(",", "");
                TotalHolderText += PressedKey;
                TotalHolderText = new StringBuilder(TotalHolderText).insert(TotalHolderText.length() - 2, ",").toString();

                if (TotalHolderText.length() > 4) {
                    if (TotalHolderText.substring(0, 1) == "0") {
                        TotalHolderText = TotalHolderText.substring(1, TotalHolderText.length() - 1);
                    }
                }
            }
        }
        if (TotalHolderText.length() == 5)

        {
            if (TotalHolderText.substring(0, 1) == "0") {
                TotalHolderText = TotalHolderText.substring(1, TotalHolderText.length() - 1);
            }
        }
        return TotalHolderText;
    }

    public void VoidTicket() {
        TextView AmountEditText = findViewById(R.id.totalAmount);
        boolean IsSaleSucceed = false;
        String Amount = AmountEditText.getText().toString().replace(",", ".");
        if (Amount.equals(""))
            Amount = "0";

        Double NetAmount = Double.parseDouble(Amount);
        if (NetAmount > 0) {
            if (MainActivity.settingsModel.ShowSignature) {
                Intent intentt = new Intent(MainActivity.this, CaptureSignature.class);
                startActivityForResult(intentt, SIGNATURE_ACTIVITY);
            }
            if (bluetoothSPP.isBluetoothAvailable()) {
                if (bluetoothSPP.getServiceState() == BluetoothState.STATE_CONNECTED) {
                    if (progress_dialog_launched) {
                        progress_dialog.cancel();
                        progress_dialog_launched = false;
                    }

                    Amount = NetAmount.toString();
                    if (!Amount.contains(".")) {
                        Amount += ".00";
                    }

                    String[] amountSplited = Amount.replace(".", ",").split(",");
                    String amountLeft = amountSplited[0];
                    String amountRight = amountSplited[1];
                    if (amountRight.length() < 02)
                        amountRight += "0";

                    amountLeft = "0000000000".substring(amountLeft.length()) + amountLeft;

                    AsyncTaskForPOSMessages AsyncMsg = new AsyncTaskForPOSMessages();
                    AsyncMsg.Amount = Double.parseDouble(Amount.replace(",", "."));
                    AsyncMsg.StartMessage = getString(R.string.VoidStarting);
                    AsyncMsg.EndMessage = getString(R.string.VoidStarted);

                    AsyncMsg.ecrCmdRequest = new WCBtProtSend();
                    AsyncMsg.ecrCmdRequest.byTrxType = WCBluetoothProtocol.WC_TX_TYPE_VOID;
                    AsyncMsg.ecrCmdRequest.iSaleAmt = Integer.parseInt(Amount.replace(".", "").replace(",", ""));
                    AsyncMsg.ecrCmdRequest.iSaleCbAmt = 0;
                    AsyncMsg.ecrCmdRequest.iRcpNo = 0;
                    AsyncMsg.ecrCmdRequest.byCommandType = WCBluetoothProtocol.WC_DO_FINANCIAL_TRX;
//
//                    AsyncMsg.ResultCode = "MOL31";
//                    AsyncMsg.SendMessagesToPOS.add("\u0002MOL30." + amountLeft + amountRight + "\u0003\u0000");

                    AsyncMsg.execute();

                } else {
                    new MaterialDialog.Builder(this)
                            .theme(Theme.LIGHT)
                            .title(R.string.Warning)
                            .content(R.string.NodeviceconnectedPleaseselectadeviceandretryafterconnected)
                            .limitIconToDefaultSize()
                            .iconRes(R.drawable.cancel_asdsad)
                            .limitIconToDefaultSize()
                            .cancelable(false)
                            .positiveText(R.string.OK)
                            .onAny(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    if (which.name().equals("POSITIVE")) {
                                        //  dialog.setProgressPercentFormat(NumberFormat.getNumberInstance());
                                        if (bluetoothSPP.isBluetoothAvailable()) {
                                            Intent intent = new Intent(getApplicationContext(), DeviceList.class);
                                            startActivityForResult(DeviceListIntent, BluetoothState.REQUEST_CONNECT_DEVICE);
                                        }
                                    }
                                }
                            })
                            .show();
                }
            }
            if (IsSaleSucceed) {
                //AppHelper.materialDialogCustom(MainActivity.getAppContext(),"Sale succeed", "", R.drawable.okay_05_2191542, 3000, "OKAY");
            }

        } else {
            new MaterialDialog.Builder(this)
                    .theme(Theme.LIGHT)
                    .title(getString(R.string.minAmountMustBiggerZero))
                    .limitIconToDefaultSize()
                    .iconRes(R.drawable.cancel_asdsad)
                    .positiveText(R.string.OK)
                    .show();
        }
    }

    public void RefundTicket() {

        TextView AmountEditText = findViewById(R.id.totalAmount);
        boolean IsSaleSucceed = false;
        String Amount = AmountEditText.getText().toString().replace(",", ".");
        if (Amount.equals(""))
            Amount = "0";

        Double NetAmount = Double.parseDouble(Amount);
        if (NetAmount > 0) {
            if (MainActivity.settingsModel.ShowSignature) {
                Intent intentt = new Intent(MainActivity.this, CaptureSignature.class);
                startActivityForResult(intentt, SIGNATURE_ACTIVITY);
            }
            if (bluetoothSPP.isBluetoothAvailable()) {
                if (bluetoothSPP.getServiceState() == BluetoothState.STATE_CONNECTED) {
                    if (progress_dialog_launched) {
                        progress_dialog.cancel();
                        progress_dialog_launched = false;
                    }

                    Amount = NetAmount.toString();
                    if (!Amount.contains(".")) {
                        Amount += ".00";
                    }

                    String[] amountSplited = Amount.replace(".", ",").split(",");
                    String amountLeft = amountSplited[0];
                    String amountRight = amountSplited[1];
                    if (amountRight.length() < 02)
                        amountRight += "0";

                    amountLeft = "0000000000".substring(amountLeft.length()) + amountLeft;

                    AsyncTaskForPOSMessages AsyncMsg = new AsyncTaskForPOSMessages();
                    AsyncMsg.Amount = Double.parseDouble(Amount.replace(",", "."));
                    AsyncMsg.StartMessage = getString(R.string.RefundStarting);
                    AsyncMsg.EndMessage = getString(R.string.RefundStarted);

                    AsyncMsg.execute();

                } else {
                    new MaterialDialog.Builder(this)
                            .theme(Theme.LIGHT)
                            .title(R.string.Warning)
                            .content(R.string.NodeviceconnectedPleaseselectadeviceandretryafterconnected)
                            .limitIconToDefaultSize()
                            .iconRes(R.drawable.cancel_asdsad)
                            .limitIconToDefaultSize()
                            .cancelable(false)
                            .positiveText(R.string.OK)
                            .onAny(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    if (which.name().equals("POSITIVE")) {
                                        if (bluetoothSPP.isBluetoothAvailable()) {
                                            Intent intent = new Intent(getApplicationContext(), DeviceList.class);
                                            startActivityForResult(DeviceListIntent, BluetoothState.REQUEST_CONNECT_DEVICE);
                                        }
                                    }
                                }
                            })
                            .show();
                }
            }
            if (IsSaleSucceed) {
                //AppHelper.materialDialogCustom(MainActivity.getAppContext(),"Sale succeed", "", R.drawable.okay_05_2191542, 3000, "OKAY");
            }

        } else {
            new MaterialDialog.Builder(this)
                    .theme(Theme.LIGHT)
                    .title(getString(R.string.minAmountMustBiggerZero))
                    .limitIconToDefaultSize()
                    .iconRes(R.drawable.cancel_asdsad)
                    .positiveText(R.string.OK)
                    .show();
        }
    }

    public void doSettlement(View view) {

        boolean IsSaleSucceed = false;
        String Amount = "0.01";
        if (Amount.equals(""))
            Amount = "0";

        Double NetAmount = Double.parseDouble(Amount);
        if (NetAmount > 0) {
            if (MainActivity.settingsModel.ShowSignature) {
                Intent intentt = new Intent(MainActivity.this, CaptureSignature.class);
                startActivityForResult(intentt, SIGNATURE_ACTIVITY);
            }
            if (bluetoothSPP.isBluetoothAvailable()) {
                if (bluetoothSPP.getServiceState() == BluetoothState.STATE_CONNECTED) {
                    if (progress_dialog_launched) {
                        progress_dialog.cancel();
                        progress_dialog_launched = false;
                    }

                    Amount = NetAmount.toString();
                    if (!Amount.contains(".")) {
                        Amount += ".00";
                    }

                    String[] amountSplited = Amount.replace(".", ",").split(",");
                    String amountLeft = amountSplited[0];
                    String amountRight = amountSplited[1];
                    if (amountRight.length() < 02)
                        amountRight += "0";

                    amountLeft = "0000000000".substring(amountLeft.length()) + amountLeft;

                    AsyncTaskForPOSMessages AsyncMsg = new AsyncTaskForPOSMessages();
                    AsyncMsg.Amount = Double.parseDouble(Amount.replace(",", "."));
                    AsyncMsg.StartMessage = getString(R.string.SettlementStarting);
                    AsyncMsg.EndMessage = getString(R.string.SettlementStarted);
                    AsyncMsg.execute();

                } else {
                    new MaterialDialog.Builder(this)
                            .theme(Theme.LIGHT)
                            .title(R.string.Warning)
                            .content(R.string.NodeviceconnectedPleaseselectadeviceandretryafterconnected)
                            .limitIconToDefaultSize()
                            .iconRes(R.drawable.cancel_asdsad)
                            .limitIconToDefaultSize()
                            .cancelable(false)
                            .positiveText(R.string.OK)
                            .onAny(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    if (which.name().equals("POSITIVE")) {
                                        if (bluetoothSPP.isBluetoothAvailable()) {
                                            Intent intent = new Intent(getApplicationContext(), DeviceList.class);
                                            startActivityForResult(DeviceListIntent, BluetoothState.REQUEST_CONNECT_DEVICE);
                                        }
                                    }
                                }
                            })
                            .show();
                }
            }
            if (IsSaleSucceed) {
                //AppHelper.materialDialogCustom(MainActivity.getAppContext(),"Sale succeed", "", R.drawable.okay_05_2191542, 3000, "OKAY");
            }

        } else {
            new MaterialDialog.Builder(this)
                    .theme(Theme.LIGHT)
                    .title(getString(R.string.minAmountMustBiggerZero))
                    .limitIconToDefaultSize()
                    .iconRes(R.drawable.cancel_asdsad)
                    .positiveText(R.string.OK)
                    .show();
        }
    }


    public void GetSettlements(String JsonData) {
        try {
            getSupportActionBar().setTitle(R.string.settlement);

            View view = inflater.inflate(R.layout.content_settlement, mainLayout, false);
            view.setTag("settlement");
            mainLayout.removeAllViews();
            mainLayout.addView(view);

            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<SHCLOG>>() {
            }.getType();
            transactions = new Gson().fromJson(JsonData, listType);

            final ListView listView = findViewById(R.id.settlementListView);
            TransactionAdapter transactionAdapter = new TransactionAdapter(this, transactions);
            listView.setAdapter(transactionAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        transaction = transactions.get(position);
                        if (AppHelper.isNetworkAvailable(MainActivity.this)) {
                            final ProgressDialog pDialog;
                            pDialog = new ProgressDialog(MainActivity.this);
                            pDialog.setMessage(getString(R.string.please_wait));
                            pDialog.setIndeterminate(true);
                            pDialog.setCancelable(false);
                            pDialog.create();
                            pDialog.show();
                            new Thread() {
                                public void run() {
                                    MainActivity.this.runOnUiThread(new Runnable() {

                                        final String ticketContent = AppHelper.createTicket(transaction);

                                        public void run() {
                                            pDialog.cancel();
                                            if (ticketContent.isEmpty())
                                                Toast.makeText(MainActivity.this, getString(R.string.Error), Toast.LENGTH_SHORT).show();
                                            else
                                                ShowTicketDetails(getString(R.string.Ticket), ticketContent);
                                        }
                                    });
                                }
                            }.start();
                        } else {
                            Toast.makeText(MainActivity.this, getString(R.string.Nointernetconnection), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception ex) {
                        //Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            final SearchView sv = findViewById(R.id.settlementFilter);

            sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String text) {
                    // TODO Auto-generated method stub
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String text) {
                    FilterTransactions(text, "settlement");
                    return false;
                }
            });
        } catch (Exception ex) {
            //Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void settingsSaveChanges(View view) {
        try {
            Gson gson = new Gson();

            btInit = true;
            savedSettings = true;

            MainActivity.settingsModel.ShowSignature = false; //((Switch) findViewById(R.id.signatureProcess)).isChecked();
            MainActivity.settingsModel.LogoffWhenExit = ((Switch) findViewById(R.id.exitAppProcess)).isChecked();
            // MainActivity.settingsModel.GetNotification = ((Switch) findViewById(R.id.getNotification)).isChecked();

            final Switch sw = findViewById(R.id.getNotification);

            sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    final MaterialDialog materialDialog = new MaterialDialog.Builder(MainActivity.this)
                            .titleColorRes(R.color.red_IOS)
                            .limitIconToDefaultSize()
                            // .content(getString(R.string.notification_active))
                            .checkBoxPromptRes(R.string.notification_active, sw.isChecked(), new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked == true) {
                                        MainActivity.settingsModel.GetNotification = true;
                                        Toast.makeText(getBaseContext(), getString(R.string.notification_active), Toast.LENGTH_LONG).show();
                                    } else if (isChecked == false) {
                                        MainActivity.settingsModel.GetNotification = false;
                                        Toast.makeText(getBaseContext(), getString(R.string.notification_deactivate), Toast.LENGTH_LONG).show();
                                    }
                                }
                            })
                            .show();
                }
            });

            MainActivity.settingsModel.ExitConfirmation = true; //((Switch) findViewById(R.id.exitConfirmation)).isChecked();

            RadioGroup radioGroup = findViewById(R.id.languageRadioGroup);
            RadioButton rb = findViewById(radioGroup.getCheckedRadioButtonId());
            MainActivity.settingsModel.Language = rb.getTag().toString();

            (new FileHelper()).WriteToFile(this, "settings.data", gson.toJson(MainActivity.settingsModel));

            Locale locale = new Locale("ro", "RO");
            if (MainActivity.settingsModel.Language.equals("en")) {
                locale = new Locale("en", "US");
            } else {
                locale = new Locale("ro", "RO");
            }
            Configuration configuration = getBaseContext().getResources().getConfiguration();
            Locale.setDefault(locale);
            configuration.locale = locale;
            getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());
        } catch (Exception ex) {
            new MaterialDialog
                    .Builder(this)
                    .title(R.string.Error)
                    .content(ex.toString())
                    .iconRes(R.drawable.okay_flat_colored)
                    .limitIconToDefaultSize()
                    .positiveColorRes(R.color.red_IOS)
                    .positiveText(R.string.OK)
                    .show();
        }
    }

    public void SettingButtonAction() {
        try {
            getSupportActionBar().setTitle(R.string.settings);
            View layout = inflater.inflate(R.layout.content_settings, mainLayout, false);
            layout.setTag("settings");
            mainLayout.removeAllViews();
            mainLayout.addView(layout);

            Gson gson = new Gson();

            MainActivity.settingsModel = gson.fromJson((new FileHelper()).ReadFromFile(MainActivity.this, "settings.data"), SettingsModel.class);

            //((Switch) findViewById(R.id.signatureProcess)).setChecked(MainActivity.settingsModel.ShowSignature);
            //((Switch) findViewById(R.id.exitConfirmation)).setChecked(MainActivity.settingsModel.ExitConfirmation);
            ((Switch) findViewById(R.id.exitAppProcess)).setChecked(MainActivity.settingsModel.LogoffWhenExit);
            ((Switch) findViewById(R.id.getNotification)).setChecked(MainActivity.settingsModel.GetNotification);
            AlertDialog.Builder myDialog = new AlertDialog.Builder(MainActivity.this);
            myDialog.setTitle("title");
            myDialog.setMessage("message");
            myDialog.setView(mainLayout);

            if (MainActivity.settingsModel.Language.equals("en"))
                ((RadioButton) findViewById(R.id.radio_en)).setChecked(true);
            else
                ((RadioButton) findViewById(R.id.radio_ro)).setChecked(true);

            //MainActivity.terminalModel = gson.fromJson((new FileHelper()).ReadFromFile(MainActivity.this, "terminal.data"), ModelTerminal.class);
            ((TextView) findViewById(R.id.TextViewSalePointID)).setText(getString(R.string.Merchant) + " ID    : " + Integer.toString(MainActivity.terminalModel.SALEPOINTID));
            ((TextView) findViewById(R.id.TextViewTerminalID)).setText(getString(R.string.Terminal) + " ID    : " + Integer.toString(MainActivity.terminalModel.ID));
            ((TextView) findViewById(R.id.TextViewSerialNumber)).setText(getString(R.string.serial_number) + " : " + MainActivity.terminalModel.SNO);


        } catch (Exception ex) {
            Toast.makeText(this, ex.toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void ChangeLocale(View v) {

        if (MainActivity.settingsModel.Language != v.getTag().toString()) {
            langIsChanged = true;
            MainActivity.settingsModel.Language = v.getTag().toString();
        } else {
            langIsChanged = false;
        }
        settingsSaveChanges(v);
    }

    public void timerDelayRemoveDialog(long time, final MaterialDialog materialDialog) {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                materialDialog.cancel();
            }
        }, time);
    }

    public void onResume() {
        super.onResume();
        //continuousTcpClient.start();
    }

    public void onStop() {
        super.onStop();
        //continuousTcpClient.stop();
    }

    //Asynctask Class
    public class AsyncTaskForPOSMessages extends AsyncTask<String, Integer, String> {

        public ProgressDialog progress_dialog;
        public WCBtProtSend ecrCmdRequest = null;
        public WCBtProtReceive ecrCmdResponse = null;
        public String StartMessage = "";
        public String EndMessage = "";
        public double Amount = 0;
        public ArrayList<String> SendMessagesToPOS = new ArrayList<String>();
        public int MessageStepCount = 0;
        public String ResultCode = "";
        public int TimeOutSecond = 60;
        private int LoopCounter = 0;


        public AsyncTaskForPOSMessages() {

        }

        @Override
        protected void onPreExecute() {
            progress_dialog = ProgressDialog.show(MainActivity.getAppContext(), getString(R.string.proces), StartMessage + "");
        }

        @Override
        protected String doInBackground(String... urls) {
            MainActivity.bluetoothReceivedMessage = "";
            publishProgress();
            String result = "false";
            while (true) {
                try {


                    if (MainActivity.bluetoothSPP.getIsResponseAvailable()) {
                        ecrCmdResponse = MainActivity.bluetoothSPP.getResponse();
                        if (ecrCmdResponse != null) {
                            result = "ok";
                            break;
                        }else {
                            result = "nok";
                            break;
                        }
                    }
                    if (LoopCounter >= TimeOutSecond)
                        break;

                    Thread.sleep(1000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LoopCounter++;
            }
            return result;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            //Added by Sorin Padure
            MainActivity.bluetoothSPP.addRequest(this.ecrCmdRequest);
            //MainActivity.bluetoothSPP.send(SendMessagesToPOS.get(MessageStepCount), true); //Commented by Sorin Padure
            //Toast.makeText(MainActivity.getAppContext(), SendMessagesToPOS.get(MessageStepCount), Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPostExecute(String res) {

            String strTitle = "";
            if (res == "ok") {

                if (ecrCmdResponse != null) {
                    if (ecrCmdResponse.isTrxResponse) {
                        EndMessage ="";
                        if(ecrCmdResponse.sRespCode.toLowerCase().equals("00".toLowerCase())){
                            strTitle = MainActivity.context.getString(R.string.tx_resp_APPROVED);
                            EndMessage += MainActivity.context.getString(R.string.tx_resp_RRN)+" : "+ecrCmdResponse.sRefNo+"\r\n" ;
                            EndMessage += MainActivity.context.getString(R.string.tx_resp_AUTH_CODE)+" : "+ecrCmdResponse.sAuthCode+"\r\n" ;
                            EndMessage += MainActivity.context.getString(R.string.tx_resp_RCP_NO)+" : "+ecrCmdResponse.iRcpNo+"\r\n" ;
                        } else {
                            strTitle = MainActivity.context.getString(R.string.tx_resp_DECLINED);
                        }
                        ecrCmdResponse = null;
                    }else {
                        strTitle = MainActivity.context.getString(R.string.tx_resp_DECLINED);
                        EndMessage =  MainActivity.context.getString(R.string.tx_resp_NO_RESP);
                    }
                } else {
                    strTitle = MainActivity.context.getString(R.string.tx_resp_DECLINED);
                    EndMessage =  MainActivity.context.getString(R.string.tx_resp_RESP_ERR);
                }

                //Toast.makeText(MainActivity.getAppContext(), "Dismiss", Toast.LENGTH_SHORT).show();
                new MaterialDialog
                        .Builder(MainActivity.getAppContext())
                        .title(strTitle)
                        .content(EndMessage)
                        .cancelable(false)
                        .positiveText(R.string.OK)
                        .onAny(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                View layout = MainActivity.inflater.inflate(R.layout.content_main, MainActivity.mainLayout, false);
                                layout.setTag("main");
                                MainActivity.mainLayout.removeAllViews();
                                MainActivity.mainLayout.addView(layout);
                                // MainActivity.this.getSupportActionBar().setTitle(R.string.app_name);
                            }
                        })
                        .show();
            } else {
                //Toast.makeText(MainActivity.getAppContext(), res, Toast.LENGTH_SHORT).show();
                new MaterialDialog
                        .Builder(MainActivity.getAppContext())
                        .content(R.string.timedOut)
                        .cancelable(false)
                        .positiveText(R.string.OK)
                        .show();
            }

            if (this.progress_dialog != null) {
                this.progress_dialog.dismiss();
            }
        }

    }

}