package ro.cloudandsun.paymentapplication;
/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.UUID;

import ro.cloudandsun.paymentapplication.BluetoothProtocol.HexUtils;
import ro.cloudandsun.paymentapplication.BluetoothProtocol.WCBluetoothProtocol;
import ro.cloudandsun.paymentapplication.BluetoothProtocol.WCBtProtReceive;
import ro.cloudandsun.paymentapplication.BluetoothProtocol.WCBtProtSend;
import ro.cloudandsun.paymentapplication.BluetoothProtocol.WCEthernetConnect;

@SuppressLint("NewApi")
public class BluetoothService {
    // Debugging
    private static final String TAG = "Bluetooth Service";

    // Name for the SDP record when creating server socket
    private static final String NAME_SECURE = "Bluetooth Secure";

    // Unique UUID for this application
    private static final UUID UUID_ANDROID_DEVICE =
            UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
    private static final UUID UUID_OTHER_DEVICE =
            UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // Member fields
    private final BluetoothAdapter mAdapter;
    private final Handler mHandler;
    private AcceptThread mSecureAcceptThread;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private int mState;
    private boolean isAndroid = BluetoothState.DEVICE_ANDROID;

    // Constructor. Prepares a new BluetoothChat session
    // context : The UI Activity Context
    // handler : A Handler to send messages back to the UI Activity
    public BluetoothService(Context context, Handler handler) {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mState = BluetoothState.STATE_NONE;
        mHandler = handler;
    }


    // Set the current state of the chat connection
    // state : An integer defining the current connection state
    public synchronized void setState(int state) {
        Log.d(TAG, "setState() " + mState + " -> " + state);
        mState = state;

        // Give the new state to the Handler so the UI Activity can update
        mHandler.obtainMessage(BluetoothState.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
    }

    // Return the current connection state. 
    public synchronized int getState() {
        return mState;
    }

    // Start the chat service. Specifically start AcceptThread to begin a
    // session in listening (server) mode. Called by the Activity onResume() 
    public synchronized void start(boolean isAndroid) {
        // Cancel any thread attempting to make a connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }
        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        setState(BluetoothState.STATE_LISTEN);

        // Start the thread to listen on a BluetoothServerSocket
        if (mSecureAcceptThread == null) {
            mSecureAcceptThread = new AcceptThread(isAndroid);
            mSecureAcceptThread.start();
            this.isAndroid = isAndroid;
        }
    }

    // Start the ConnectThread to initiate a connection to a remote device
    // device : The BluetoothDevice to connect
    // secure : Socket Security type - Secure (true) , Insecure (false)
    public synchronized void connect(BluetoothDevice device) {
        // Cancel any thread attempting to make a connection
        if (mState == BluetoothState.STATE_CONNECTING) {
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
        setState(BluetoothState.STATE_CONNECTING);
    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     *
     * @param socket The BluetoothSocket on which the connection was made
     * @param device The BluetoothDevice that has been connected
     */
    public synchronized void connected(BluetoothSocket socket, BluetoothDevice
            device, final String socketType) {
        // Cancel the thread that completed the connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Cancel the accept thread because we only want to connect to one device
        if (mSecureAcceptThread != null) {
            mSecureAcceptThread.cancel();
            mSecureAcceptThread = null;
        }

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(socket, socketType, mHandler);
        mConnectedThread.start();

        // Send the name of the connected device back to the UI Activity
        Message msg = mHandler.obtainMessage(BluetoothState.MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putString(BluetoothState.DEVICE_NAME, device.getName());
        bundle.putString(BluetoothState.DEVICE_ADDRESS, device.getAddress());
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        setState(BluetoothState.STATE_CONNECTED);
    }

    // Stop all threads
    public synchronized void stop() {
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        if (mSecureAcceptThread != null) {
            mSecureAcceptThread.cancel();
            mSecureAcceptThread.kill();
            mSecureAcceptThread = null;
        }
        setState(BluetoothState.STATE_NONE);
    }

    // Write to the ConnectedThread in an unsynchronized manner
    // out : The bytes to write
    public void write(byte[] out) {
        // Create temporary object
        ConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != BluetoothState.STATE_CONNECTED) return;
            r = mConnectedThread;
        }
        // Perform the write unsynchronized
        r.write(out);
    }

    // Add request in an unsynchronized manner
    // int : ECR Request to be added
    public void setRequest(WCBtProtSend a_ecrCmdRequest) {
        // Create temporary object
        ConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != BluetoothState.STATE_CONNECTED) return;
            r = mConnectedThread;
        }
        // Perform the set unsynchronized
        r.setEcrCmdRequest(a_ecrCmdRequest);
    }

    // Gets response in an unsynchronized manner
    // int : ECR Request to be added
    public WCBtProtReceive getResponse() {
        // Create temporary object
        ConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != BluetoothState.STATE_CONNECTED) return null;
            r = mConnectedThread;
        }
        // Perform the set unsynchronized
        return r.getEcrCmdResponse();
    }

    public boolean getIsResponseAvailable() {
        // Create temporary object
        ConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != BluetoothState.STATE_CONNECTED) return false;
            r = mConnectedThread;
        }
        // Perform the set unsynchronized
        return r.isEcrRespIsAvailable();
    }

    // Indicate that the connection attempt failed and notify the UI Activity
    private void connectionFailed() {
        // Start the service over to restart listening mode
        this.start(this.isAndroid);
    }

    // Indicate that the connection was lost and notify the UI Activity
    private void connectionLost() {
        // Start the service over to restart listening mode
        this.start(this.isAndroid);
    }

    // This thread runs while listening for incoming connections. It behaves
    // like a server-side client. It runs until a connection is accepted
    // (or until cancelled)
    private class AcceptThread extends Thread {
        // The local server socket
        private BluetoothServerSocket mmServerSocket;
        private String mSocketType;
        boolean isRunning = true;

        public AcceptThread(boolean isAndroid) {
            BluetoothServerSocket tmp = null;

            // Create a new listening server socket
            try {
                if (isAndroid)
                    tmp = mAdapter.listenUsingRfcommWithServiceRecord(NAME_SECURE, UUID_ANDROID_DEVICE);
                else
                    tmp = mAdapter.listenUsingRfcommWithServiceRecord(NAME_SECURE, UUID_OTHER_DEVICE);
            } catch (IOException e) {
            }
            mmServerSocket = tmp;
        }

        public void run() {
            setName("AcceptThread" + mSocketType);
            BluetoothSocket socket = null;

            // Listen to the server socket if we're not connected
            while (mState != BluetoothState.STATE_CONNECTED && isRunning) {
                try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    break;
                }

                // If a connection was accepted
                if (socket != null) {
                    synchronized (this) {
                        switch (mState) {
                            case BluetoothState.STATE_LISTEN:
                            case BluetoothState.STATE_CONNECTING:
                                // Situation normal. Start the connected thread.
                                connected(socket, socket.getRemoteDevice(),
                                        mSocketType);
                                break;
                            case BluetoothState.STATE_NONE:
                            case BluetoothState.STATE_CONNECTED:
                                // Either not ready or already connected. Terminate new socket.
                                try {
                                    socket.close();
                                } catch (IOException e) {
                                }
                                break;
                        }
                    }
                }
            }
        }

        public void cancel() {
            try {
                mmServerSocket.close();
                mmServerSocket = null;
            } catch (IOException e) {
            }
        }

        public void kill() {
            isRunning = false;
        }
    }


    // This thread runs while attempting to make an outgoing connection
    // with a device. It runs straight through
    // the connection either succeeds or fails
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private String mSocketType;

        public ConnectThread(BluetoothDevice device) {
            mmDevice = device;
            BluetoothSocket tmp = null;

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                if (isAndroid)
                    tmp = device.createRfcommSocketToServiceRecord(UUID_ANDROID_DEVICE);
                else
                    tmp = device.createRfcommSocketToServiceRecord(UUID_OTHER_DEVICE);
            } catch (IOException e) {
            }
            mmSocket = tmp;
        }

        public void run() {
            // Always cancel discovery because it will slow down a connection
            mAdapter.cancelDiscovery();

            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                mmSocket.connect();
            } catch (IOException e) {
                // Close the socket
                try {
                    mmSocket.close();
                } catch (IOException e2) {
                }
                connectionFailed();
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (this) {
                mConnectThread = null;
            }

            // Start the connected thread
            connected(mmSocket, mmDevice, mSocketType);
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
            }
        }
    }

    // This thread runs during a connection with a remote device.
    // It handles all incoming and outgoing transmissions.
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final Handler mHandler;
        private final InputStream mmInputStream;
        private final OutputStream mmOutputStream;
        private volatile boolean ecrReqIsAvailable = false;
        private volatile boolean ecrRespIsAvailable = false;
        private volatile WCBtProtSend ecrCmdRequest = null;
        private volatile WCBtProtReceive ecrCmdResponse = null;

        public ConnectedThread(BluetoothSocket socket, String socketType, Handler handler) {
            mmSocket = socket;
            mHandler = handler;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
            }

            mmInputStream = tmpIn;
            mmOutputStream = tmpOut;
        }

        private void sendDisconnededEvent(int iDeviceStatus) {
            mHandler.obtainMessage(BluetoothState.MESSAGE_STATE_CHANGE, BluetoothState.STATE_DISCONNECTED, -1).sendToTarget();
        }

        public void run() {
            byte[] buffer = new byte[WCBluetoothProtocol.WC_MSG_DATA_MAX_LEN];  // buffer store for the stream
            int bytesAvailable; // bytes returned from read()
            //Added by Sorin Padure
            byte[] packetBytes;
            WCBluetoothProtocol bthProtocol = new WCBluetoothProtocol();
            WCBtProtReceive wcBtProtReceive;
            WCBtProtSend wcBtProtResponse;
            WCEthernetConnect wcEthernetConnect = null;

            boolean bSessionStarted = false; // session started flag
            long lSessionStart = System.currentTimeMillis() / 1000;// session start timestamp in seconds
            long lSessionTimeout = 60;//session timeout in seconds


            while (true) {

                if (!mmSocket.isConnected()) {
                    sendDisconnededEvent(-1);
                    ecrReqIsAvailable = true;
                    this.ecrCmdResponse = null;
                }

                //If session is started
                if (bSessionStarted) {
                    //If session time was reached it means that POS terminal has encountered an error
                    if (System.currentTimeMillis() / 1000 > lSessionStart + lSessionTimeout) {
                        //Reset session flag
                        bSessionStarted = false;
                    }
                }

                try {
                    bytesAvailable = mmInputStream.available();
                    packetBytes = new byte[WCBluetoothProtocol.WC_MSG_DATA_MAX_LEN];
                    //Log.d("comm" , "bytes available ["+bytesAvailable+"]" );
                    if (bytesAvailable > 0) {
                        Log.d("main", "bytes available [" + bytesAvailable + "]");
                        packetBytes = new byte[bytesAvailable];
                        mmInputStream.read(packetBytes);
                        System.out.println(HexUtils.formatHexDump(packetBytes, 0, bytesAvailable));
                        //Build up response
                        wcBtProtReceive = new WCBtProtReceive(packetBytes, bytesAvailable);
                        wcBtProtResponse = bthProtocol.ProcessRequest(wcBtProtReceive);

                        if (wcBtProtResponse != null) {
                            boolean isCmdSuccess = false;
                            switch (wcBtProtResponse.byCommandType) {
                                case WCBluetoothProtocol.WC_DO_OPEN_CMD:
                                    if (wcEthernetConnect != null) {
                                        wcEthernetConnect.disconnect();
                                    }
                                    wcEthernetConnect = new WCEthernetConnect();
                                    isCmdSuccess = true;
                                    if (isCmdSuccess) {
                                        Log.d("main", "WC_DO_OPEN_CMD -> ACK");
                                        mmOutputStream.write(WCBluetoothProtocol.ACK);
                                    } else {
                                        Log.d("main", "WC_DO_OPEN_CMD -> NAK");
                                        mmOutputStream.write(WCBluetoothProtocol.NAK);
                                    }
                                    mmOutputStream.flush();
                                    bSessionStarted = true;
                                    lSessionStart = System.currentTimeMillis() / 1000;
                                    break;
                                case WCBluetoothProtocol.WC_DO_CONNECT_CMD:
                                    if (wcEthernetConnect == null) {
                                        wcEthernetConnect = new WCEthernetConnect();
                                    }
                                    wcEthernetConnect.setmIpAddress(wcBtProtResponse.btProtConnection.szHostIP);
                                    wcEthernetConnect.setmPort(Integer.valueOf(wcBtProtResponse.btProtConnection.szHostPort));
                                    wcEthernetConnect.setiSendTimeout(wcBtProtResponse.btProtConnection.iSendTimeout);
                                    wcEthernetConnect.setiRecvTimeout(wcBtProtResponse.btProtConnection.iRecvTimeout);
                                    wcEthernetConnect.setiConnTimeout(wcBtProtResponse.btProtConnection.iConnTimeout);

                                    isCmdSuccess = true;
                                    if (wcEthernetConnect.init()) {
                                        if (wcEthernetConnect.connect()) {
                                            isCmdSuccess = true;
                                        }
                                    }

                                    if (isCmdSuccess) {
                                        Log.d("main", "WC_DO_CONNECT_CMD -> ACK");
                                        mmOutputStream.write(WCBluetoothProtocol.ACK);
                                    } else {
                                        Log.d("main", "WC_DO_CONNECT_CMD -> NAK");
                                        mmOutputStream.write(WCBluetoothProtocol.NAK);
                                    }
                                    mmOutputStream.flush();
                                    break;

                                case WCBluetoothProtocol.WC_DO_SEND_ONLY_CMD:
                                    if (wcEthernetConnect != null) {
                                        if (!wcEthernetConnect.ismIsConnected()) {
                                            wcEthernetConnect.connect();
                                        }
                                        if (wcEthernetConnect.ismIsConnected()) {

                                            isCmdSuccess = wcEthernetConnect.send(wcBtProtResponse.dataBuffer, wcBtProtResponse.iDataBufferLen);
                                            if (isCmdSuccess) {

                                                Log.d("main", "WC_DO_SEND_ONLY_CMD -> ACK");
                                                mmOutputStream.write(WCBluetoothProtocol.ACK);
                                                mmOutputStream.flush();
                                            }
                                        }
                                    } else {
                                        mmOutputStream.write(WCBluetoothProtocol.EOT);
                                        mmOutputStream.flush();
                                    }
                                    break;
                                case WCBluetoothProtocol.WC_DO_RECV_ONLY_CMD:
                                    Log.d("main", "WC_DO_RECV_ONLY_CMD -> ACK");
                                    mmOutputStream.write(WCBluetoothProtocol.ACK);
                                    mmOutputStream.flush();
                                    if (wcEthernetConnect != null) {
                                        if (wcEthernetConnect.ismIsConnected()) {
                                            wcBtProtResponse.dataBuffer = new byte[WCBluetoothProtocol.WC_MSG_DATA_MAX_LEN];
                                            wcBtProtResponse.iDataBufferLen = wcEthernetConnect.receive(wcBtProtResponse.dataBuffer, wcBtProtResponse.iDataBufferLen);
                                        }
                                        if (wcBtProtResponse.iDataBufferLen > 0) {
                                            //Pack response data
                                            if (bthProtocol.PackResponse(wcBtProtResponse)) {

                                                Log.d("main", "Sending back data ");
                                                Log.d("main", HexUtils.formatHexDump(wcBtProtResponse.dataBuffer, 0, wcBtProtResponse.iDataBufferLen));
                                                mmOutputStream.write(wcBtProtResponse.dataBuffer, 0, wcBtProtResponse.iDataBufferLen);
                                                mmOutputStream.flush();

                                                int iMaxTimeout = 300;
                                                int iTimeout = 0;
                                                while ((bytesAvailable = mmInputStream.available()) == 0 && iTimeout < iMaxTimeout) {
                                                    Thread.sleep(5);
                                                    if (bytesAvailable > 0) {
                                                        packetBytes = new byte[bytesAvailable];
                                                        mmInputStream.read(packetBytes);
                                                        if (packetBytes[0] == WCBluetoothProtocol.ACK) {
                                                            Log.d("main", "ACK received");
                                                            break;
                                                        } else {
                                                            Log.d("main", "WRONG Data received");
                                                            Log.d("main", HexUtils.formatHexDump(packetBytes, 0, packetBytes.length));
                                                            break;
                                                        }
                                                    }
                                                    iTimeout++;
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case WCBluetoothProtocol.WC_DO_SEND_RECV_P_CMD:
                                case WCBluetoothProtocol.WC_DO_SEND_RECV_U_CMD:
                                    if (wcEthernetConnect != null) {
                                        if (!wcEthernetConnect.ismIsConnected()) {
                                            wcEthernetConnect.connect();
                                        }
                                        if (wcEthernetConnect.ismIsConnected()) {

                                            isCmdSuccess = wcEthernetConnect.send(wcBtProtResponse.dataBuffer, wcBtProtResponse.iDataBufferLen);
                                            if (isCmdSuccess) {

                                                Log.d("main", "WC_DO_SEND_RECV_P_CMD/WC_DO_SEND_RECV_U_CMD -> ACK");
                                                mmOutputStream.write(WCBluetoothProtocol.ACK);
                                                mmOutputStream.flush();

                                                wcBtProtResponse.dataBuffer = new byte[WCBluetoothProtocol.WC_MSG_DATA_MAX_LEN];
                                                wcBtProtResponse.iDataBufferLen = wcEthernetConnect.receive(wcBtProtResponse.dataBuffer, wcBtProtResponse.dataBuffer.length);
                                                if (wcBtProtResponse.iDataBufferLen > 0) {
                                                    //Pack response data
                                                    if (bthProtocol.PackResponse(wcBtProtResponse)) {

                                                        Log.d("main", "Sending back data ");
                                                        Log.d("main", HexUtils.formatHexDump(wcBtProtResponse.dataBuffer, 0, wcBtProtResponse.iDataBufferLen));
                                                        mmOutputStream.write(wcBtProtResponse.dataBuffer, 0, wcBtProtResponse.iDataBufferLen);
                                                        mmOutputStream.flush();

                                                        int iMaxTimeout = 30;
                                                        int iTimeout = 0;
                                                        while ((bytesAvailable = mmInputStream.available()) == 0 && iTimeout < iMaxTimeout) {
                                                            Thread.sleep(50);
                                                            Log.d("main", "Waiting for ACK ");
                                                            if (bytesAvailable > 0) {
                                                                packetBytes = new byte[bytesAvailable];
                                                                mmInputStream.read(packetBytes);
                                                                if (packetBytes[0] == WCBluetoothProtocol.ACK) {
                                                                    Log.d("main", "ACK received");
                                                                    break;
                                                                } else {
                                                                    Log.d("main", "WRONG Data received");
                                                                    Log.d("main", HexUtils.formatHexDump(packetBytes, 0, packetBytes.length));
                                                                    break;
                                                                }
                                                            }
                                                            iTimeout++;
                                                        }
                                                    }
                                                }
                                            } else {
                                                Log.d("main", "WC_DO_SEND_RECV_P_CMD/WC_DO_SEND_RECV_U_CMD -> EOT");
                                                mmOutputStream.write(WCBluetoothProtocol.EOT);
                                                mmOutputStream.flush();
                                            }
                                        }
                                    }
                                    break;
                                case WCBluetoothProtocol.WC_DO_DISCONNECT:
                                    if (wcEthernetConnect != null) {
                                        isCmdSuccess = wcEthernetConnect.disconnect();
                                    } else {
                                        isCmdSuccess = true;
                                    }
                                    if (isCmdSuccess) {
                                        Log.d("main", "WC_DO_DISCONNECT -> ACK");
                                        mmOutputStream.write(WCBluetoothProtocol.ACK);
                                    } else {
                                        Log.d("main", "WC_DO_DISCONNECT -> NAK");
                                        mmOutputStream.write(WCBluetoothProtocol.NAK);
                                    }
                                    mmOutputStream.flush();
                                    bSessionStarted = false;
                                    break;
                                case WCBluetoothProtocol.WC_DO_CLOSE_CMD:
                                    isCmdSuccess = true;
                                    if (isCmdSuccess) {
                                        Log.d("main", "WC_DO_CLOSE_CMD -> ACK");
                                        mmOutputStream.write(WCBluetoothProtocol.ACK);
                                    } else {
                                        Log.d("main", "WC_DO_CLOSE_CMD -> NAK");
                                        mmOutputStream.write(WCBluetoothProtocol.NAK);
                                    }
                                    mmOutputStream.flush();
                                    bSessionStarted = false;
                                    break;
                                case WCBluetoothProtocol.WC_DO_FINANCIAL_TRX:
                                    isCmdSuccess = true;
                                    ecrCmdResponse = wcBtProtReceive;
                                    ecrCmdResponse.byTrxType = wcBtProtResponse.byTrxType;
                                    ecrCmdResponse.iSaleAmt = wcBtProtResponse.iSaleAmt;
                                    ecrCmdResponse.iSaleCbAmt = wcBtProtResponse.iSaleCbAmt;
                                    ecrCmdResponse.iRcpNo = wcBtProtResponse.iRcpNo;
                                    ecrCmdResponse.sAuthCode = wcBtProtResponse.sAuthCode;
                                    ecrCmdResponse.sRefNo = wcBtProtResponse.sRefNo;
                                    ecrCmdResponse.sRespCode = wcBtProtResponse.sRespCode;
                                    ecrCmdResponse.isTrxResponse = true;
                                    ecrRespIsAvailable = true;

                                    if (isCmdSuccess) {
                                        Log.d("main", "WC_DO_FINANCIAL_TRX -> ACK");
                                        mmOutputStream.write(WCBluetoothProtocol.ACK);
                                    } else {
                                        Log.d("main", "WC_DO_FINANCIAL_TRX -> NAK");
                                        mmOutputStream.write(WCBluetoothProtocol.NAK);
                                    }
                                    mmOutputStream.flush();
                                    break;
                            }


                        }
                    } else {
                        if (ecrReqIsAvailable) {

                            if (bSessionStarted) {
                                ecrReqIsAvailable = true;
                                this.ecrCmdResponse = null;
                                this.ecrRespIsAvailable = true;
                                continue;
                            } else {
                                ecrReqIsAvailable = false;
                                this.ecrCmdResponse = null;
                                this.ecrRespIsAvailable = false;
                                //Pack response
                                if (bthProtocol.PackResponse(ecrCmdRequest)) {
                                    try {
                                        Log.d("main", HexUtils.formatHexDump(ecrCmdRequest.dataBuffer, 0, ecrCmdRequest.iDataBufferLen));

                                        mmOutputStream.write(ecrCmdRequest.dataBuffer, 0, ecrCmdRequest.iDataBufferLen);
                                        mmOutputStream.flush();

                                        int iMaxTimeout = 300;
                                        int iTimeout = 0;
                                        while (iTimeout < iMaxTimeout) {
                                            bytesAvailable = mmInputStream.available();
                                            Log.d("main", "Waiting for ACK ");
                                            if (bytesAvailable > 0) {
                                                packetBytes = new byte[bytesAvailable];
                                                mmInputStream.read(packetBytes);
                                                if (packetBytes[0] == WCBluetoothProtocol.ACK) {
                                                    Log.d("main", "ACK received");
                                                    break;
                                                } else {
                                                    Log.d("main", "WRONG Data received");
                                                    Log.d("main", HexUtils.formatHexDump(packetBytes, 0, packetBytes.length));
                                                    break;
                                                }
                                            }
                                            Thread.sleep(10);
                                            iTimeout++;
                                        }
                                    } catch (IOException ex) {
                                        Log.d("main", "Bluetooth on sending request error -> " + ex.getMessage());
                                        if (mmSocket.isConnected()) {
                                            if (ex.getMessage().toLowerCase().contains("Broken pipe".toLowerCase())) {
                                                mmSocket.close();
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    this.sendDisconnededEvent(-1);
                    ecrReqIsAvailable = true;
                    this.ecrCmdResponse = null;
                    break;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }

        // Write to the connected OutStream.
        // @param buffer  The bytes to write
        public void write(byte[] buffer) {
            try {
                mmOutputStream.write(buffer);
                // Share the sent message back to the UI Activity
                mHandler.obtainMessage(BluetoothState.MESSAGE_WRITE
                        , -1, -1, buffer).sendToTarget();
            } catch (IOException e) {
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
            }
        }

        public boolean isEcrRespIsAvailable() {
            return ecrRespIsAvailable;
        }

        public WCBtProtReceive getEcrCmdResponse() {
            ecrRespIsAvailable = false;
            return ecrCmdResponse;
        }

        public void setEcrReqIsAvailable(boolean ecrReqIsAvailable) {
            this.ecrReqIsAvailable = ecrReqIsAvailable;
        }

        public void setEcrCmdRequest(WCBtProtSend ecrCmdRequest) {
            this.ecrCmdRequest = ecrCmdRequest;
            this.ecrReqIsAvailable = true;
        }
    }
}
