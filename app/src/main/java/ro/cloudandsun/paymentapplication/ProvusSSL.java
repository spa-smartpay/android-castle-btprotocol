package ro.cloudandsun.paymentapplication;

import android.util.Log;

import org.apache.http.conn.ssl.SSLSocketFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManagerFactory;

/**
 * @author rawad
 */
public class ProvusSSL {

    /**
     * @throws IOException
     * @throws UnknownHostException
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws CertificateException
     * @throws KeyStoreException
     * @throws UnrecoverableKeyException
     */
    public static final String IP = "193.104.123.14";
    public static final int TCP_PORT = 5566;
    static boolean timeOut = false;

    public static String formatHexDump(byte[] array, int offset, int length) {
        final int width = 16;

        StringBuilder builder = new StringBuilder();

        for (int rowOffset = offset; rowOffset < offset + length; rowOffset += width) {
            builder.append(String.format("%06d:  ", rowOffset));

            for (int index = 0; index < width; index++) {
                if (rowOffset + index < array.length) {
                    builder.append(String.format("%02x ", array[rowOffset + index]));
                } else {
                    builder.append("   ");
                }
            }

            if (rowOffset < array.length) {
                int asciiWidth = Math.min(width, array.length - rowOffset);
                builder.append("  |  ");
                try {
                    builder.append(new String(array, rowOffset, asciiWidth, "UTF-8").replaceAll("\r\n", " ").replaceAll("\n", " "));
                } catch (UnsupportedEncodingException ignored) {
                    //If UTF-8 isn't available as an encoding then what can we do?!
                }
            }

            builder.append(String.format("%n"));
        }

        return builder.toString();
    }

    /**
     * @return
     * @throws KeyStoreException
     * @throws NoSuchAlgorithmException
     * @throws CertificateException
     * @throws IOException
     * @throws UnrecoverableKeyException
     */
    private static KeyManagerFactory factor(KeyStore keyStore, String algorithm) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableKeyException {
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(algorithm);
        keyManagerFactory.init(keyStore, "password".toCharArray());
        System.out.println("key manager initiated");

        return keyManagerFactory;
    }

    private static TrustManagerFactory trustManager(KeyStore keyStore) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
        TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        instance.init(keyStore);
        return instance;

    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public int readShort(byte[] data, int offset) {
        int ch1 = data[offset] & 0xff;
        int ch2 = data[offset + 1] & 0xff;
        return (ch1 << 8) + (ch2 << 0);
    }

    public String SSLConnection(InputStream keystoreInputStream, String algorithm, String MSG, byte[] byteData) throws NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException, UnrecoverableKeyException, KeyManagementException {
        System.out.println("TLS loading keystore");
        KeyStore keyStore = KeyStore.getInstance("BKS");
        keyStore.load(keystoreInputStream, "password".toCharArray());
        System.out.println("TLS keystore loaded");

        SSLSocketFactory socketFactory = new SSLSocketFactory(keyStore);
        socketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        SSLSocket socket = (SSLSocket) socketFactory.createSocket(new Socket(IP, TCP_PORT), IP, TCP_PORT, false);
        socket.setEnabledProtocols(new String[]{"TLSv1.2"});
        System.out.println("TLS connecting");
        socket.startHandshake();

        socket.setKeepAlive(true);
        System.out.println("TLS connected");
        System.out.println("TLS writing test");

        Log.d("TLS MessageToSSL", MSG);

        if (byteData.length > 0) {
            String sendToTls = formatHexDump(byteData, 0, byteData.length);
            //Log.d("TLS SEND DATA BYTE ARRAY:", sendToTls);
            socket.getOutputStream().write(byteData);
        } else {
            Log.d("TLS SEND DATA ASC", MSG);
            socket.getOutputStream().write(hexStringToByteArray(MSG));
        }

        System.out.println("TLS test message written, fetching response");
        InputStream inputStream = socket.getInputStream();

        String response = "";

        byte[] InData = new byte[900];
        int InDataCnt = 0;
        int FindLen = 0;

        int x = 0;

        long start_time = System.currentTimeMillis();
        long wait_time = 15000;
        long end_time = start_time + wait_time;


        while ((x = inputStream.read()) != -1) {
            response = response + ((char) x);

            InData[InDataCnt] = ((byte) x);
            InDataCnt++;

            if (InDataCnt == 2) {
                FindLen = readShort(InData, 0);
                Log.d("TLS Reci+5eved LEN", "" + FindLen);
            }

            if (InDataCnt >= FindLen + 2) {
                Log.d("TLS FIND LEN", "" + FindLen);
                Log.d("TLS INDATA LEN", "" + InDataCnt);

                String FromTlsData = formatHexDump(InData, 0, InData.length);
                Log.d("TLS END DATA ", FromTlsData);

                break;
            }

            if (System.currentTimeMillis() > end_time) {
                System.out.println("TLS time out.");
                break;
            }
        }

        String textAsc = new String(InData, 0, InDataCnt, "ASCII");             // CONVERT TO ASCII FOR RETURN

//        String responseFromSSLSocket = formatHexDump(response.getBytes(), 0, response.length() + 1);
        String responseFromSSLSocket = formatHexDump(InData, 0, InDataCnt);
        Log.d("TLS From TLS SSL Socket", responseFromSSLSocket);


//ASC!        MainActivity.bluetoothSPP.send(responseFromSSLSocket, false);

        MainActivity.bluetoothSPP.sendBtBynary(InData, InDataCnt, false);


        socket.close();

        Log.d("TLS END ASC DATA", textAsc);

        return textAsc;

    }

}
