package ro.cloudandsun.paymentapplication;

import java.util.List;


public class ModelTransaction {
    private String RRN;

    private String Amount;

    private String TxnStatus;

    private String CreatedDate;

    private String ReceiptNumber;

    private String BatchID;

    private String TxnType;

    private String CardType;

    private String TxnDateTime;

    private String Currency;

    private String CardNo;

    public String getRRN() {
        return RRN;
    }

    public void setRRN(String RRN) {
        this.RRN = RRN;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String Amount) {
        this.Amount = Amount;
    }

    public String getTxnStatus() {
        return TxnStatus;
    }

    public void setTxnStatus(String TxnStatus) {
        this.TxnStatus = TxnStatus;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    public String getReceiptNumber() {
        return ReceiptNumber;
    }

    public void setReceiptNumber(String ReceiptNumber) {
        this.ReceiptNumber = ReceiptNumber;
    }

    public String getBatchID() {
        return BatchID;
    }

    public void setBatchID(String BatchID) {
        this.BatchID = BatchID;
    }

    public String getTxnType() {
        return TxnType;
    }

    public void setTxnType(String TxnType) {
        this.TxnType = TxnType;
    }

    public String getCardType() {
        return CardType;
    }

    public void setCardType(String CardType) {
        this.CardType = CardType;
    }

    public String getTxnDateTime() {
        return TxnDateTime;
    }

    public void setTxnDateTime(String TxnDateTime) {
        this.TxnDateTime = TxnDateTime;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String Currency) {
        this.Currency = Currency;
    }

    public String getCardNo() {
        return CardNo;
    }

    public void setCardNo(String CardNo) {
        this.CardNo = CardNo;
    }

    @Override
    public String toString() {
        return "ClassPojo [RRN = " + RRN + ", Amount = " + Amount + ", TxnStatus = " + TxnStatus + ", CreatedDate = " + CreatedDate + ", ReceiptNumber = " + ReceiptNumber + ", BatchID = " + BatchID + ", TxnType = " + TxnType + ", CardType = " + CardType + ", TxnDateTime = " + TxnDateTime + ", Currency = " + Currency + ", CardNo = " + CardNo + "]";
    }
}
