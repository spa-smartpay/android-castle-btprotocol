package ro.cloudandsun.paymentapplication;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;
import android.widget.VideoView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.scottyab.rootbeer.RootBeer;

import java.io.File;
import java.util.Locale;

public class FirstScreen extends Activity {

    public static Boolean btIsEnabled = false;
    VideoView mVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_screen);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        RootBeer rootBeer = new RootBeer(this);

        if (rootBeer.isRooted() && 1 > 1) {
            new MaterialDialog.Builder(FirstScreen.this)
                    .titleColorRes(R.color.red_IOS)
                    .limitIconToDefaultSize()
                    .content(getString(R.string.DeviceRooted))
                    .positiveText(R.string.OK)
                    .positiveColorRes(R.color.red)
                    .cancelable(false)
                    .onAny(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            if (which.name().equals("POSITIVE")) {
                                finish();
                            }
                        }
                    })
                    .show();
        } else {
            Gson gson = new Gson();

            if (fileExist("settings.data")) {
                SettingsModel settingsModel = gson.fromJson((new FileHelper()).ReadFromFile(this, "settings.data"), SettingsModel.class);
                Locale locale = new Locale(settingsModel.Language);
                Configuration configuration = getBaseContext().getResources().getConfiguration();
                Locale.setDefault(locale);
                configuration.locale = locale;
                getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());
            } else {
                Locale locale = new Locale("ro", "RO");
                Configuration configuration = getBaseContext().getResources().getConfiguration();
                Locale.setDefault(locale);
                configuration.locale = locale;
                getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());
            }

            getWindow().setFormat(PixelFormat.UNKNOWN);
            mVideoView = findViewById(R.id.video_view);
            String uriPath = "android.resource://" + getPackageName() + "/" + R.raw.v52;
            Uri uri = Uri.parse(uriPath);
            mVideoView.setVideoURI(uri);
            mVideoView.requestFocus();
            mVideoView.start();

            mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mVideoView.start(); //need to make transition seamless.
                }
            });

            Thread mSplashThread;
            mSplashThread = new Thread() {
                @Override
                public void run() {
                    try {
                        synchronized (this) {
                            final Gson gson = new Gson();

                            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                            if (bluetoothAdapter != null) {
                                if (!bluetoothAdapter.isEnabled()) {
                                    bluetoothAdapter.enable();
                                    btIsEnabled = true;
                                }
                            }

                            wait(2000);

                            String terminalData = (new FileHelper()).ReadFromFile(FirstScreen.this, "terminal.data");
                            MainActivity.terminalModel = (new Gson()).fromJson(terminalData, ModelTerminal.class);

                            SettingsModel settingsModel = null;

                            if ((new FileHelper()).fileExist("settings.data", FirstScreen.this)) {
                                settingsModel = gson.fromJson((new FileHelper()).ReadFromFile(FirstScreen.this, "settings.data"), SettingsModel.class);
                            }

                            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            if (sharedPreferences.getBoolean("AgreementApproved", false) == false) {
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                                finish();
                            } else if (settingsModel == null) {
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                                finish();
                            } else if (!settingsModel.LogoffWhenExit) {
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                if (MainActivity.terminalModel != null) {
                                    final String terminal = AppHelper.GetDataFromURL("https://posmobil.vodafone.ro/api/Terminal/LoginTerminal/" + MainActivity.terminalModel.SNO);
                                    final ModelTerminal modelTerminal = gson.fromJson(terminal, ModelTerminal.class);
                                    MainActivity.terminalModel = gson.fromJson(terminal, ModelTerminal.class);

                                    if (modelTerminal != null) {
                                        (new FileHelper()).WriteToFile(FirstScreen.this, "terminal.data", terminal);
                                        if (modelTerminal.STATUS) {
                                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    } else {
                                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                } else {
                                    wait(2000);
                                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        }
                    } catch (Exception ex) {
                        //Toast.makeText(FirstScreen.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                        Log.d("DUNDAR", ex.toString());
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            };
            mSplashThread.start();
        }
    }

    public boolean fileExist(String fileName) {
        String path = getApplicationContext().getFilesDir().getAbsolutePath() + "/" + fileName;
        File file = new File(path);
        return file.exists();
    }
}
