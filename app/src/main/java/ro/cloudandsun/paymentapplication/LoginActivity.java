package ro.cloudandsun.paymentapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class LoginActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_BEHIND);

        Button button = (Button) findViewById(R.id.LoginButton);
        button.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.LoginButton:
                if (AppHelper.isNetworkAvailable(this)) {
                    String SNO = ((EditText) findViewById(R.id.LoginSerialNumber)).getText().toString();
                    String URL = "https://posmobil.vodafone.ro/api/terminal/LoginTerminal/" + SNO;
                    if (SNO.length() > 0)
                        new LongOperation().execute(URL);
                    else
                        Toast.makeText(this, getString(R.string.pleaseEnterSerialNumber), Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(this, getString(R.string.Nointernetconnection), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public void LoginToApp(String JsonData) {

        try {
            Gson gson = new Gson();
            ModelTerminal terminal = new Gson().fromJson(JsonData, ModelTerminal.class);

            if (terminal == null) {
                Toast.makeText(this, getString(R.string.Serialnumberisincorrect), Toast.LENGTH_LONG).show();
                return;
            }

            boolean IsUserExist = false;
            ProgressDialog pDialog = AppHelper.showProgressDialog(this, getString(R.string.please_wait));
            boolean wResult = (new FileHelper()).WriteToFile(this, "terminal.data", gson.toJson(terminal));
            pDialog.cancel();
            IsUserExist = true;

            final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            if (sharedPreferences.getBoolean("AgreementApproved", false) == false) {

                WebView webView = new WebView(this);
                webView.loadUrl("file:///android_asset/Agreement.htm");

                MaterialDialog materialDialog = new MaterialDialog.Builder(LoginActivity.this)
                        .titleColorRes(R.color.red_IOS)
                        .limitIconToDefaultSize()
                        .customView(webView, true)
                        .positiveText(getString(R.string.acord))
                        .positiveColorRes(R.color.green_IOS)
                        .neutralText(getString(R.string.neacord))
                        .neutralColorRes(R.color.red)
                        .cancelable(false)
                        .onAny(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                if (which.name().equals("POSITIVE")) {

                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putBoolean("AgreementApproved", true);

                                    editor.commit();

                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        })
                        .show();
            } else {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }

            if (!IsUserExist) {
                new MaterialDialog.Builder(this)
                        .content(getString(R.string.Serialnumberisincorrect))
                        .limitIconToDefaultSize()
                        .iconRes(R.drawable.cancel)
                        .positiveText(getString(R.string.OK))
                        .positiveColorRes(R.color.red)
                        .show();
            }
        } catch (Exception ex) {
            //Toast.makeText(this, ex.toString(), Toast.LENGTH_LONG).show();
            Toast.makeText(this, "Please try again.", Toast.LENGTH_LONG).show();
        }
    }

    private class LongOperation extends AsyncTask<String, Integer, String> {

        ProgressDialog pDialog;
        Button button = (Button) findViewById(R.id.LoginButton);

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage(getString(R.string.please_wait));
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.create();
            pDialog.show();
        }

        protected String doInBackground(String... arg0) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return AppHelper.GetDataFromURL(arg0[0]);
        }

        protected void onProgressUpdate(Integer... a) {
            super.onProgressUpdate(a);
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            pDialog.cancel();
            LoginToApp(result);
            button.setText(R.string.login);
        }
    }
}