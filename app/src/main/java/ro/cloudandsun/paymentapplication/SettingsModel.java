package ro.cloudandsun.paymentapplication;

/**
 * Created by LG on 29.09.2017.
 */

public class SettingsModel {

    public Boolean ShowSignature;
    public Boolean LogoffWhenExit;
    public Boolean GetNotification;
    public Boolean ExitConfirmation;
    public String Language;

}
