package ro.cloudandsun.paymentapplication;

/**
 * Created by LG on 21.09.2017.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

public class CaptureSignatureActivity extends Activity {

    public static final int SIGNATURE_ACTIVITY = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SIGNATURE_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status = bundle.getString("status");
                    if (status.equalsIgnoreCase("done")) {
                        /*
                        Toast toast = Toast.makeText(this, "Sale succeed!", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP, 105, 50);
                        toast.show();
                        new MaterialDialog
                                .Builder(this).content("Sale succeed")
                                .positiveColorRes(R.color.red_IOS)
                                .positiveText(R.string.OK)
                                .show();
                        */
                    }
                }
                break;
        }

    }

}