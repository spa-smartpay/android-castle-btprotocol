package ro.cloudandsun.paymentapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.pdf.PdfDocument;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.print.PrintHelper;
import android.view.View;

import com.afollestad.bridge.Bridge;
import com.afollestad.bridge.Form;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by DUNDAR on 17.09.2017.
 */

public class AppHelper {

    public AppHelper() {

    }

    public static MaterialDialog materialDialogCustom(final Context context, final String title, final String content, final int icon, int duration, final String positiveText) {

        final MaterialDialog materialDialog = new MaterialDialog
                .Builder(context)
                .theme(Theme.LIGHT)
                .title(title)
                .content(content)
                .content(content)
                .iconRes(icon)
                .limitIconToDefaultSize()
                .positiveColorRes(R.color.blue_IOS)
                .positiveText(positiveText).build();

        final MaterialDialog progress_dialog = new MaterialDialog.Builder(context)
                .progress(true, 0)
                .cancelable(false)
                .content(R.string.please_wait)
                .build();

        progress_dialog.create();
        progress_dialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progress_dialog.dismiss();
                progress_dialog.cancel();
                materialDialog.show();
            }
        }, duration);


        return materialDialog;

    }

    public static String GetDataFromURL(String Url) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String JsonUserData = "";
        try {
            URL url = new URL(Url);
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), "UTF8"));
            String inputLine;
            while ((inputLine = in.readLine()) != null)
                JsonUserData += inputLine;
            in.close();
        } catch (Exception e) {
        }
        return JsonUserData;
    }

    public static ProgressDialog showProgressDialog(Context context, String MessageText) {
        ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage(MessageText);
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(true);
        pDialog.create();
        pDialog.show();
        return pDialog;
    }

    public static Date ConvertToDate(String dateString, String dateFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat); // "MM/dd/yyyy hh:mm:ss aa"
        Date convertedDate = new Date();
        try {
            convertedDate = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public static String createTicket(SHCLOG transaction) {
        String Ticket = "";
        try {
            SHCLOG trans = transaction;

            Form form = new Form()
                    .add("MID", MainActivity.terminalModel.SALEPOINTREFID)
                    .add("TID", MainActivity.terminalModel.REFID)
                    .add("RRN", transaction.getRRN())
                    .add("ReceiptNumber", transaction.getReceiptNumber())
                    .add("token", MainActivity.terminalModel.TOKEN);

            final String JsonResult = Bridge
                    .post("https://posmobil.vodafone.ro/api/transactions/get")
                    .body(form)
                    .request().response().asString();

            Gson gson = new Gson();
            transaction = gson.fromJson(JsonResult, SHCLOG.class);
            Ticket = "" +
                    "Merchant Name: " + transaction.getMerchantName() + "<br>" +
                    "Merchant Address: " + transaction.getMerchantAddress() + "<br>" +
                    "MID: " + transaction.getMID() + "<br>" +
                    "TID: " + transaction.getTID() + "<br>" +
                    "Tranzaction Type: " + transaction.getTxnType() + "<br>" +
                    "Card Type: " + transaction.getCardType() + "<br>" +
                    "Truncated Pan: " + transaction.getCardNo().substring(0, 5) + "******" + transaction.getCardNo().substring(12, 16) + "<br>" +
                    "Application Lable: Vodafone MPOS<br>" +
                    "Batch Number: " + transaction.getBatchID() + "<br>" +
                    "Receipt Number: " + trans.getReceiptNumber() + "<br>" +
                    "RRN: " + trans.getRRN() + "<br>" +
                    "STAN: " + transaction.getTrace() + "<br>" +
                    "Date, Hour: " + transaction.getCreatedDate() + "<br>" +
                    "<b>Amount: " + Double.parseDouble(transaction.getAmount()) + "</b><br>" +
                    "Currency: " + "LEI" + "<br>" + //transaction.getCurrency() +
                    //"FIELD FOR SIGNATURE (IF APROPRIATE): NONE<br>" +
                    "Authorization Result: " + transaction.getRespCode() + "<br>" +
                    "Status: " + transaction.getRespCode() + "<br>" +
                    "Authorization Code: " + transaction.getAuthNum() + "<br>" +
                    "";

        } catch (Exception ex) {
            //Toast.makeText(MainActivity.getAppContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return Ticket;
    }

    public static String escapeHtml(CharSequence text) {
        StringBuilder out = new StringBuilder();
        withinStyle(out, text, 0, text.length());
        return out.toString();
    }

    private static void withinStyle(StringBuilder out, CharSequence text,
                                    int start, int end) {
        for (int i = start; i < end; i++) {
            char c = text.charAt(i);

            if (c == '<') {
                out.append("&lt;");
            } else if (c == '>') {
                out.append("&gt;");
            } else if (c == '&') {
                out.append("&amp;");
            } else if (c >= 0xD800 && c <= 0xDFFF) {
                if (c < 0xDC00 && i + 1 < end) {
                    char d = text.charAt(i + 1);
                    if (d >= 0xDC00 && d <= 0xDFFF) {
                        i++;
                        int codepoint = 0x010000 | (int) c - 0xD800 << 10 | (int) d - 0xDC00;
                        out.append("&#").append(codepoint).append(";");
                    }
                }
            } else if (c > 0x7E || c < ' ') {
                out.append("&#").append((int) c).append(";");
            } else if (c == ' ') {
                while (i + 1 < end && text.charAt(i + 1) == ' ') {
                    out.append("&nbsp;");
                    i++;
                }

                out.append(' ');
            } else {
                out.append(c);
            }
        }
    }

    public static Bitmap getScreenShot(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    public static void doPhotoPrint(Resources res, Bitmap bitmap) {
        PrintHelper photoPrinter = new PrintHelper(MainActivity.getAppContext());
        photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);
        //Bitmap bitmap = BitmapFactory.decodeResource(res, R.drawable.background_vodafone);
        photoPrinter.printBitmap("background_vodafone.jpg - test print", bitmap);
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            // Network is present and connected
            isAvailable = true;
        }
        return isAvailable;
    }

    public static Bitmap takeScreenshot(View rootView) {

        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }

    public static void drawPage(PdfDocument.Page page) {
        Canvas canvas = page.getCanvas();

        // units are in points (1/72 of an inch)
        int titleBaseLine = 72;
        int leftMargin = 54;

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(36);
        canvas.drawText("Test Title", leftMargin, titleBaseLine, paint);

        paint.setTextSize(11);
        canvas.drawText("Test paragraph", leftMargin, titleBaseLine + 25, paint);

        paint.setColor(Color.BLUE);
        canvas.drawRect(100, 100, 172, 172, paint);
    }

    public static String readAssetFile(AssetManager mgr, String path) {
        String contents = "";
        InputStream is = null;
        BufferedReader reader = null;
        try {
            is = mgr.open(path);
            reader = new BufferedReader(new InputStreamReader(is));
            contents = reader.readLine();
            String line = null;
            while ((line = reader.readLine()) != null) {
                contents += '\n' + line;
            }
        } catch (final Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ignored) {
                }
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignored) {
                }
            }
        }
        return contents;
    }

    private String saveToInternalStorage(Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(MainActivity.getAppContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, "profile.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    private Bitmap loadImageFromStorage(String path) {

        try {
            File f = new File(path, "profile.jpg");
            return BitmapFactory.decodeStream(new FileInputStream(f));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }

    }
}


