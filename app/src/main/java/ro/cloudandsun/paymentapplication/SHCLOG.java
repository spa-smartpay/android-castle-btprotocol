
package ro.cloudandsun.paymentapplication;

    public class SHCLOG {
        private String RRN;

        private String TxnStatus;

        private String Amount;

        private String Trace;

        private String BatchID;

        private String ReceiptNumber;

        private String TxnDateTime;

        private String MID;

        private String MerchantName;

        private String CardNo;

        private String ReceiptNo;

        private String RespCode;

        private String TID;

        private String AuthNum;

        private String CreatedDate;

        private String TxnType;

        private String CardType;

        private String MerchantAddress;

        private String Currency;

        private String AppLabel;

        private String MsgType;

        public String getMsgType() {
            return MsgType;
        }

        public void setMsgType(String msgType) {
            this.MsgType = msgType;
        }


        public String getRRN() {
            return RRN;
        }

        public void setRRN(String RRN) {
            this.RRN = RRN;
        }

        public String getTxnStatus() {
            return TxnStatus;
        }

        public void setTxnStatus(String TxnStatus) {
            this.TxnStatus = TxnStatus;
        }

        public String getAmount() {
            return Amount;
        }

        public void setAmount(String Amount) {
            this.Amount = Amount;
        }

        public String getTrace() {
            return Trace;
        }

        public void setTrace(String Trace) {
            this.Trace = Trace;
        }

        public String getBatchID() {
            return BatchID;
        }

        public void setBatchID(String BatchID) {
            this.BatchID = BatchID;
        }

        public String getReceiptNumber() {
            return ReceiptNumber;
        }

        public void setReceiptNumber(String ReceiptNumber) {
            this.ReceiptNumber = ReceiptNumber;
        }

        public String getTxnDateTime() {
            return TxnDateTime;
        }

        public void setTxnDateTime(String TxnDateTime) {
            this.TxnDateTime = TxnDateTime;
        }

        public String getMID() {
            return MID;
        }

        public void setMID(String MID) {
            this.MID = MID;
        }

        public String getMerchantName() {
            return MerchantName;
        }

        public void setMerchantName(String MerchantName) {
            this.MerchantName = MerchantName;
        }

        public String getCardNo() {
            return CardNo;
        }

        public void setCardNo(String CardNo) {
            this.CardNo = CardNo;
        }

        public String getReceiptNo() {
            return ReceiptNo;
        }

        public void setReceiptNo(String ReceiptNo) {
            this.ReceiptNo = ReceiptNo;
        }

        public String getRespCode() {
            return RespCode;
        }

        public void setRespCode(String RespCode) {
            this.RespCode = RespCode;
        }

        public String getTID() {
            return TID;
        }

        public void setTID(String TID) {
            this.TID = TID;
        }

        public String getAuthNum() {
            return AuthNum;
        }

        public void setAuthNum(String AuthNum) {
            this.AuthNum = AuthNum;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public String getTxnType() {
            return TxnType;
        }

        public void setTxnType(String TxnType) {
            this.TxnType = TxnType;
        }

        public String getCardType() {
            return CardType;
        }

        public void setCardType(String CardType) {
            this.CardType = CardType;
        }

        public String getMerchantAddress() {
            return MerchantAddress;
        }

        public void setMerchantAddress(String MerchantAddress) {
            this.MerchantAddress = MerchantAddress;
        }

        public String getCurrency() {
            return Currency;
        }

        public void setCurrency(String Currency) {
            this.Currency = Currency;
        }

        public String getAppLabel() {
            return AppLabel;
        }

        public void setAppLabel(String AppLabel) {
            this.AppLabel = AppLabel;
        }

        @Override
        public String toString() {
            return "ClassPojo [RRN = " + RRN + ", TxnStatus = " + TxnStatus + ", Amount = " + Amount + ", Trace = " + Trace + ", BatchID = " + BatchID + ", ReceiptNumber = " + ReceiptNumber + ", TxnDateTime = " + TxnDateTime + ", MID = " + MID + ", MerchantName = " + MerchantName + ", CardNo = " + CardNo + ", ReceiptNo = " + ReceiptNo + ", RespCode = " + RespCode + ", TID = " + TID + ", AuthNum = " + AuthNum + ", CreatedDate = " + CreatedDate + ", TxnType = " + TxnType + ", CardType = " + CardType + ", MerchantAddress = " + MerchantAddress + ", Currency = " + Currency + ", AppLabel = " + AppLabel + ", MsgType = "+MsgType+"]";
        }
    }
