package com.smartpay.btgateway;

import android.util.Log;

import java.io.UnsupportedEncodingException;

/**
 * Created by Forest on 21.08.2017.
 */

public class HexUtils {
    private static final String TAG = "HexUtils";

    public static byte hexChar2Byte(char var0) {
        return var0 >= 48 && var0 <= 57?(byte)(var0 - 48):(var0 >= 97 && var0 <= 102?(byte)(var0 - 97 + 10):(var0 >= 65 && var0 <= 70?(byte)(var0 - 65 + 10):-1));
    }
    public static byte[] hexString2ByteArray(String var0) {
        if(var0 != null && !var0.equals("")) {
            if(var0.length() % 2 != 0) {
                var0 = var0 + "0";
            }

            byte[] var1 = new byte[var0.length() / 2];

            for(int var2 = 0; var2 < var0.length() / 2; ++var2) {
                char var3 = var0.charAt(2 * var2);
                char var4 = var0.charAt(2 * var2 + 1);
                byte var5 = hexChar2Byte(var3);
                byte var6 = hexChar2Byte(var4);
                if(var5 < 0 || var6 < 0) {
                    return null;
                }

                int var7 = var5 << 4;
                var1[var2] = (byte)(var7 + var6);
            }

            return var1;
        } else {
            return null;
        }
    }

    public static String formatHexDump(byte[] array, int offset, int length) {
        final int width = 16;

        StringBuilder builder = new StringBuilder();

        for (int rowOffset = offset; rowOffset < offset + length; rowOffset += width) {
            builder.append(String.format("%06d:  ", rowOffset));

            for (int index = 0; index < width; index++) {
                if (rowOffset + index < array.length) {
                    builder.append(String.format("%02x ", array[rowOffset + index]));
                } else {
                    builder.append("   ");
                }
            }

            if (rowOffset < array.length) {
                int asciiWidth = Math.min(width, array.length - rowOffset);
                builder.append("  |  ");
                try {
                    builder.append(new String(array, rowOffset, asciiWidth, "UTF-8").replaceAll("\r\n", " ").replaceAll("\n", " "));
                } catch (UnsupportedEncodingException ignored) {
                    //If UTF-8 isn't available as an encoding then what can we do?!
                }
            }

            builder.append(String.format("%n"));
        }

        return builder.toString();
    }

    public static String bcdToASCII(byte[] bytes)
    {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        Log.d(TAG , "bcdToASCII : "+ sb.toString());

        return sb.toString();
    }
}
