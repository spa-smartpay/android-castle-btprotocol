package com.smartpay.btgateway.WCBluetoothProtocol;

import android.util.Log;

import com.smartpay.btgateway.HexUtils;

import java.nio.ByteBuffer;

/**
 * Created by Forest on 23.03.2018.
 */

public class WCBluetoothProtocol {

    public static final byte STX = 0x02;
    public static final byte ETX = 0x03;
    public static final byte ENQ = 0x05;
    public static final byte ACK = 0x06;
    public static final byte NAK = 0x15;
    public static final byte EOT = 0x04;

    public static final int WC_MSG_DATA_MAX_LEN = 1024;

    public static final int g_cusWCTAG_COMMAND = 0xF000;
    public static final int g_cusWCTAG_RESPONSE = 0xF100;

    public static final byte WC_DO_OPEN_CMD = (byte) 0xF1;
    public static final byte WC_DO_CONNECT_CMD =(byte) 0xF2;
    public static final byte WC_DO_SEND_RECV_P_CMD =(byte)  0xF3;
    public static final byte WC_DO_SEND_RECV_U_CMD =(byte)  0xF4;
    public static final byte WC_DO_SEND_ONLY_CMD =(byte)  0xF5;
    public static final byte WC_DO_RECV_ONLY_CMD =(byte)  0xF6;
    public static final byte WC_DO_DISCONNECT =(byte)  0xF7;
    public static final byte WC_DO_CLOSE_CMD =(byte)  0xF8;

    public static final int g_cusWCTAG_HOST_IP = 0xE001;
    public static final int g_cusWCTAG_HOST_PORT = 0xE002;
    public static final int g_cusWCTAG_HOST_DEST_TYPE = 0xE003;
    public static final int g_cusWCTAG_HOST_COMM_TYPE = 0xE004;
    public static final int g_cusWCTAG_HOST_CA_INDEX = 0xE005;
    public static final int g_cusWCTAG_HOST_CONN_TIMEOUT = 0xE006;
    public static final int g_cusWCTAG_HOST_SEND_TIMEOUT = 0xE007;
    public static final int g_cusWCTAG_HOST_RECV_TIMEOUT = 0xE008;

    public static final int g_cusWCTAG_DATA_BUFF = 0xF001;
    public static final int g_cusWCTAG_DATA_MAX_LEN = 0xF002;



    /**
     * Process received request from mPOS
     *
     * @param wcBtProtRequest
     * @return WCBtProtResponse - response data or null if invalid request received
     */
    public WCBtProtResponse ProcessRequest(WCBtProtRequest wcBtProtRequest) {
        int iCnt = 0;
        byte[] byBuff = new byte[1024];
        int iCmd = 0;
        byte byCmd = 0x00;

        WCBtProtResponse protResponse = new WCBtProtResponse();

        if (wcBtProtRequest.iDataBufferLen < 6) {
            Log.d("wcProtBt", "received data is less than 3");
            return null;
        } else {

            Log.d("wcProtBt", "received data is greater than 3");
            //Check if STX received
            if (wcBtProtRequest.dataBuffer[0] == STX) {

                Log.d("wcProtBt", "STX received");
                iCnt++;

                //Next 2 bytes are the total data size
                System.arraycopy(wcBtProtRequest.dataBuffer, iCnt, byBuff, 0, 2);
                Log.d("wcProtBt", "First 2 bytes : \n" + HexUtils.formatHexDump(byBuff, 0, 2));
                iCnt += 2;

                //Search the command tag & value
                byBuff = WCBtProtUtils.GetTagBuffer(g_cusWCTAG_COMMAND, wcBtProtRequest.dataBuffer, wcBtProtRequest.iDataBufferLen);
                if (byBuff == null || byBuff.length == 0) {
                    Log.d("wcProtBt", "Could not find command tag :");
                    return null;
                }

                Log.d("wcProtBt", "First 2 bytes : \n" + HexUtils.formatHexDump(byBuff, 0, 2));

                //Get command value
                byCmd = byBuff[0];

                //Search for ETX & CRC
                if (wcBtProtRequest.dataBuffer[wcBtProtRequest.dataBuffer.length - 3] != ETX) {
                    //ETX not received
                    return null;
                }

                //Extract CRC
                byte[] byCrc = new byte[2];
                System.arraycopy(wcBtProtRequest.dataBuffer, wcBtProtRequest.dataBuffer.length - 2, byCrc, 0, 2);
                byte[] byDataBody = new byte[wcBtProtRequest.dataBuffer.length - iCnt  - 3];
                System.arraycopy(wcBtProtRequest.dataBuffer, iCnt, byDataBody, 0, wcBtProtRequest.dataBuffer.length - iCnt - 3);
                int iRecvCRC = WCBtProtUtils.ByteArrToInt(byCrc);
                int iCalcCRC = CRC.CalculateCRC(0 , byDataBody , byDataBody.length);

                if (iRecvCRC != iCalcCRC) {
                    Log.d("wcProtBt", "CRC does not match calc [" + iCalcCRC + "] recv [" + iRecvCRC + "]\n");
                    return null;
                }

                switch (byCmd) {
                    case WC_DO_OPEN_CMD:
                        Log.d("wcProtBt", "Command [OPEN] received");
                        protResponse= wcBtProtRequest.ProcessOpenCommand();
                        break;
                    case WC_DO_CONNECT_CMD:
                        Log.d("wcProtBt", "Command [CONNECT] received");
                        protResponse = wcBtProtRequest.ProcessConnectCommand();
                        break;
                    case WC_DO_SEND_ONLY_CMD:
                        Log.d("wcProtBt", "Command [WC_DO_SEND_ONLY_CMD] received");
                        protResponse = wcBtProtRequest.ProcessSendCommand();
                        break;
                    case WC_DO_RECV_ONLY_CMD:
                        Log.d("wcProtBt", "Command [WC_DO_RECV_ONLY_CMD] received");
                        protResponse = wcBtProtRequest.ProcessRecvCommand();
                        break;
                    case WC_DO_SEND_RECV_P_CMD:
                        Log.d("wcProtBt", "Command [SEND/RECV P] received");
                        protResponse = wcBtProtRequest.ProcessSendRecvCommand_P();
                        break;
                    case WC_DO_SEND_RECV_U_CMD:
                        Log.d("wcProtBt", "Command [SEND/RECV U] received");
                        protResponse = wcBtProtRequest.ProcessSendRecvCommand_U();
                        break;
                    case WC_DO_DISCONNECT:
                        Log.d("wcProtBt", "Command [DISCONNECT] received");
                        protResponse = wcBtProtRequest.ProcessDisconnectCommand();
                        break;
                    case WC_DO_CLOSE_CMD:
                        Log.d("wcProtBt", "Command [CLOSE] received");
                        protResponse = wcBtProtRequest.ProcessCloseCommand();
                        break;
                }

            }
        }


        return protResponse;
    }

    public boolean PackResponse(WCBtProtResponse wcBtProtResponse) {
        try{
            if(wcBtProtResponse.PackResponse())
            {
                Log.d("prot" , "PackResponse OK ");
                Log.d("prot", HexUtils.formatHexDump(wcBtProtResponse.dataBuffer, 0, wcBtProtResponse.iDataBufferLen));
                return  true;
            }

            Log.d("prot" , "PackResponse NOK ");
            return false;
        }catch (Exception ex)
        {
            Log.d("prot" , "PackResponse exception : " + ex.getMessage());
            return false;
        }
    }
}
