package com.smartpay.btgateway.WCBluetoothProtocol;

import android.util.Log;


/**
 * Created by Forest on 23.03.2018.
 */

public class WCBtProtResponse {
    int iResult;
    public byte byCommandType;
    public WCPBtProtConnection btProtConnection;
    public byte[] dataBuffer;
    public int iDataBufferLen;

    public WCBtProtResponse() {
    }

    public WCBtProtResponse(byte[] a_dataBuffer, int a_iDataBufferLen) {
        this.dataBuffer = a_dataBuffer;
        this.iDataBufferLen = a_iDataBufferLen;
    }

    public boolean PackResponse() {
        boolean resp = false;
        if (this.iDataBufferLen > 0) {
            switch (this.byCommandType) {
                case WCBluetoothProtocol.WC_DO_CONNECT_CMD:
                case WCBluetoothProtocol.WC_DO_SEND_RECV_P_CMD:
                case WCBluetoothProtocol.WC_DO_SEND_RECV_U_CMD:
                case WCBluetoothProtocol.WC_DO_RECV_ONLY_CMD:
                case WCBluetoothProtocol.WC_DO_SEND_ONLY_CMD:
                    Log.d("wcProtBt - resp", "Command ["+this.byCommandType+"] received");
                    if (this.PackSendRecvCommand()) {
                        resp = true;
                    }
                    break;

            }
        }
        return resp;
    }

    private boolean PackSendRecvCommand() {

        byte[] respBuff = WCBtProtUtils.SetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_RESPONSE, this.dataBuffer, this.iDataBufferLen);
        byte[] fullRespBuff = WCBtProtUtils.AddHeaders(respBuff);
        this.dataBuffer = fullRespBuff;
        this.iDataBufferLen = fullRespBuff.length;
        if (this.dataBuffer != null && this.iDataBufferLen > 0) {
            return true;
        } else {
            return false;
        }
    }
}
