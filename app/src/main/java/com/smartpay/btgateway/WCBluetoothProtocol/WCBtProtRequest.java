package com.smartpay.btgateway.WCBluetoothProtocol;

/**
 * Created by Forest on 23.03.2018.
 */

public class WCBtProtRequest {
    public byte[] dataBuffer;
    public int iDataBufferLen;

    public WCBtProtRequest(byte[] a_dataBuffer , int a_iDataBufferLen)
    {
        this.dataBuffer = a_dataBuffer;
        this.iDataBufferLen = a_iDataBufferLen;
    }

    public WCBtProtResponse ProcessOpenCommand() {
        WCBtProtResponse response = new WCBtProtResponse();
        //Set Command type
        response.byCommandType = WCBluetoothProtocol.WC_DO_OPEN_CMD;
        return response;
    }

    public WCBtProtResponse ProcessConnectCommand() {
        int iVal = 0;
        String strBuffer;
        WCBtProtResponse response = new WCBtProtResponse();
        response.byCommandType = WCBluetoothProtocol.WC_DO_CONNECT_CMD;
        response.btProtConnection = new WCPBtProtConnection();
        //Get Host IP
        strBuffer = WCBtProtUtils.GetTagString(WCBluetoothProtocol.g_cusWCTAG_HOST_IP, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.szHostIP = strBuffer;
        }
        //Get Host Port
        strBuffer = WCBtProtUtils.GetTagString(WCBluetoothProtocol.g_cusWCTAG_HOST_PORT, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.szHostPort = strBuffer;
        }
        //Get Host Dest Type
        iVal = WCBtProtUtils.GetTagInt(WCBluetoothProtocol.g_cusWCTAG_HOST_DEST_TYPE, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.iDestType = iVal;
        }
        //Get Host Comm Type
        iVal = WCBtProtUtils.GetTagInt(WCBluetoothProtocol.g_cusWCTAG_HOST_COMM_TYPE, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.iCommType = iVal;
        }
        //Get Host Certificate of Authority Index
        iVal = WCBtProtUtils.GetTagInt(WCBluetoothProtocol.g_cusWCTAG_HOST_CA_INDEX, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.iCAIndex = iVal;
        }
        //Get Host Connecting timeout
        iVal = WCBtProtUtils.GetTagInt(WCBluetoothProtocol.g_cusWCTAG_HOST_CONN_TIMEOUT, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.iConnTimeout = iVal;
        }
        //Get Host Send timeout
        iVal = WCBtProtUtils.GetTagInt(WCBluetoothProtocol.g_cusWCTAG_HOST_SEND_TIMEOUT , this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.iSendTimeout = iVal;
        }
        //Get Host Receive timeout
        iVal = WCBtProtUtils.GetTagInt(WCBluetoothProtocol.g_cusWCTAG_HOST_RECV_TIMEOUT, this.dataBuffer, this.iDataBufferLen);
        if (strBuffer != null) {
            response.btProtConnection.iRecvTimeout = iVal;
        }
        return response;
    }

    public WCBtProtResponse ProcessRecvCommand() {
        byte[] byBuff;
        WCBtProtResponse response = new WCBtProtResponse();

        response.byCommandType = WCBluetoothProtocol.WC_DO_RECV_ONLY_CMD;
        byBuff = WCBtProtUtils.GetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_DATA_MAX_LEN , this.dataBuffer, this.iDataBufferLen);
        if(byBuff != null && byBuff.length >0)
        {
            response.byCommandType = WCBluetoothProtocol.WC_DO_RECV_ONLY_CMD;
            response.dataBuffer = new byte[byBuff.length];
            response.iDataBufferLen = WCBtProtUtils.ByteArrToInt(byBuff);
            System.arraycopy(byBuff , 0 , response.dataBuffer , 0 , byBuff.length );
            iDataBufferLen = byBuff.length;
        }

        return response;
    }

    public WCBtProtResponse ProcessSendCommand() {
        byte[] byBuff;
        WCBtProtResponse response = new WCBtProtResponse();
        response.byCommandType = WCBluetoothProtocol.WC_DO_SEND_ONLY_CMD;
        byBuff = WCBtProtUtils.GetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_DATA_BUFF , this.dataBuffer, this.iDataBufferLen);
        if(byBuff != null && byBuff.length >0)
        {
            response.byCommandType = WCBluetoothProtocol.WC_DO_SEND_ONLY_CMD;
            response.dataBuffer = new byte[byBuff.length];
            System.arraycopy(byBuff , 0 , response.dataBuffer , 0 , byBuff.length );
            response.iDataBufferLen = byBuff.length;
            iDataBufferLen = byBuff.length;
        }

        return response;
    }
    public WCBtProtResponse ProcessSendRecvCommand_P() {
        byte[] byBuff;
        WCBtProtResponse response = new WCBtProtResponse();

        byBuff = WCBtProtUtils.GetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_DATA_BUFF , this.dataBuffer, this.iDataBufferLen);
        if(byBuff != null && byBuff.length >0)
        {
            response.byCommandType = WCBluetoothProtocol.WC_DO_SEND_RECV_P_CMD;
            response.dataBuffer = new byte[byBuff.length];
            System.arraycopy(byBuff , 0 , response.dataBuffer , 0 , byBuff.length );
            response.iDataBufferLen = byBuff.length;
            iDataBufferLen = byBuff.length;
        }

        return response;
    }

    public WCBtProtResponse ProcessSendRecvCommand_U() {
        byte[] byBuff;
        WCBtProtResponse response = new WCBtProtResponse();

        byBuff = WCBtProtUtils.GetTagBuffer(WCBluetoothProtocol.g_cusWCTAG_DATA_BUFF , this.dataBuffer, this.iDataBufferLen);
        if(byBuff != null && byBuff.length >0)
        {
            response.byCommandType = WCBluetoothProtocol.WC_DO_SEND_RECV_U_CMD;
            response.dataBuffer = new byte[byBuff.length];
            System.arraycopy(byBuff , 0 , response.dataBuffer , 0 , byBuff.length );
            response.iDataBufferLen = byBuff.length;
            iDataBufferLen = byBuff.length;
        }
        return response;
    }

    public WCBtProtResponse ProcessDisconnectCommand() {
        WCBtProtResponse response = new WCBtProtResponse();
        response.byCommandType = WCBluetoothProtocol.WC_DO_DISCONNECT;
        return response;
    }

    public WCBtProtResponse ProcessCloseCommand() {
        WCBtProtResponse response = new WCBtProtResponse();
        response.byCommandType = WCBluetoothProtocol.WC_DO_CLOSE_CMD;
        return response;
    }
}
