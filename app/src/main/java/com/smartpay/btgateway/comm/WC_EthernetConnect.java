package com.smartpay.btgateway.comm;

import android.util.Log;

import com.smartpay.btgateway.HexUtils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by Forest on 11.08.2017.
 */

public class WC_EthernetConnect {
    private String mIpAddress;
    private boolean mIsConnected;
    private int mPort;
    private Socket mSocket;
    private OutputStream mOutputStream;
    private InputStream mInputStream;
    private int iSendTimeout;
    private int iRecvTimeout;
    private int iConnTimeout;


    public WC_EthernetConnect() {
    }

    public WC_EthernetConnect(String ipAddress, int iPort) {
        this.setmIpAddress(ipAddress);
        this.setmPort(iPort);
    }

    public boolean init() {
        try {
            this.config();
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean config() {
        return false;
    }

    public boolean connect() {
        try {
            this.setmSocket(new Socket(this.getmIpAddress(), this.getmPort()));
            this.setmIsConnected(true);
            return true;
        } catch (Exception ex) {
            System.out.println(ex);
            return false;
        }
    }

    public boolean send(byte[] inMsg, int len) {

        Log.d("eth","Sending ["+len+"] bytes with ["+iSendTimeout+"] seconds timeout ");
        try {
            this.getmSocket().setSoTimeout(iSendTimeout * 1000);
            this.setmOutputStream(this.getmSocket().getOutputStream());
            DataOutputStream out = new DataOutputStream(this.getmOutputStream());
            out.write(inMsg, 0, len);
            out.flush();
            Log.d("eth", "Sent data ... ");
            Log.d("eth", HexUtils.formatHexDump(inMsg, 0, len));
            return true;
        } catch (Exception ex) {
            System.out.println(ex);
            return false;
        }
    }

    public int receive(byte[] outMsg, int len) {

        Log.d("eth" , "receive max ["+len+"] bytes" );
        Log.d("eth" , "receive max ["+iRecvTimeout+"] seconds ");
        try {
            //this.getmSocket().setSoTimeout(iRecvTimeout * 1000);
            this.getmSocket().setSoTimeout(3000);

            byte[] buffer = new byte[4096];
            byte recvByte;
            int iCnt = 0;
            this.setmInputStream(this.getmSocket().getInputStream());
            DataInputStream in = new DataInputStream(this.getmInputStream());
            int iEstRecvCnt = in.available();
            Log.d("eth" , "initial estimated available ["+iEstRecvCnt+"] bytes" );

            long startTime = System.currentTimeMillis();
            while(iEstRecvCnt == 0 && (System.currentTimeMillis()-startTime)< iRecvTimeout*1000)
            {
                iEstRecvCnt = in.available();
            }

            Log.d("eth" , "final estimated available ["+iEstRecvCnt+"] bytes" );
            while(iCnt < (Math.min(len , iEstRecvCnt)))
            {
                try {
                    //iEstRecvCnt = in.available();
                    recvByte = in.readByte();
                    buffer[iCnt] = recvByte;
                    iCnt++;
                }catch (Exception ex)
                {
                    Log.d("eth", "Receiving exception : "+ ex);
                    break;
                }
            }

//            while ((iReadedBytes = in.read(buffer)) > 0) {
////                System.arraycopy(buffer, 0, outMsg, 0, iReadedBytes);
////                Log.d("eth" , "received "+iReadedBytes+" bytes");
////                Log.d("eth", HexUtils.formatHexDump(buffer, 0, iReadedBytes));
////                iTtlBytesReceived += iReadedBytes;
////                if(iTtlBytesReceived >= len)
////                {
////                    iTtlBytesReceived = len;
////                    break;
////                }
////            }
            System.arraycopy(buffer, 0, outMsg, 0, iCnt);

            len = iCnt;
            Log.d("eth", "Received data ... [" + len+ "] bytes len");
            Log.d("eth" , HexUtils.formatHexDump(outMsg, 0, len));
            if(len > 0 ) {
                return iCnt;
            }else {
                return -1;
            }
        } catch (Exception ex) {
            System.out.println(ex);
            return -1;
        }
    }

    public boolean disconnect() {
        try {
            this.getmSocket().close();
            this.setmIsConnected(false);
            return true;
        } catch (Exception ex) {
            System.out.println(ex);
            return false;
        }
    }

    public String getmIpAddress() {
        return mIpAddress;
    }

    public void setmIpAddress(String mIpAddress) {
        this.mIpAddress = mIpAddress;
    }

    public int getmPort() {
        return mPort;
    }

    public void setmPort(int mPort) {
        this.mPort = mPort;
    }

    public Socket getmSocket() {
        return mSocket;
    }

    public void setmSocket(Socket mSocket) {
        this.mSocket = mSocket;
    }

    public OutputStream getmOutputStream() {
        return mOutputStream;
    }

    public void setmOutputStream(OutputStream mOutputStream) {
        this.mOutputStream = mOutputStream;
    }

    public InputStream getmInputStream() {
        return mInputStream;
    }

    public void setmInputStream(InputStream mInputStream) {
        this.mInputStream = mInputStream;
    }

    public int getiSendTimeout() {
        return iSendTimeout;
    }

    public void setiSendTimeout(int iSendTimeout) {
        this.iSendTimeout = iSendTimeout;
    }

    public int getiRecvTimeout() {
        return iRecvTimeout;
    }

    public void setiRecvTimeout(int iRecvTimeout) {
        this.iRecvTimeout = iRecvTimeout;
    }

    public int getiConnTimeout() {
        return iConnTimeout;
    }

    public void setiConnTimeout(int iConnTimeout) {
        this.iConnTimeout = iConnTimeout;
    }

    public boolean ismIsConnected() {
        return mIsConnected;
    }

    public void setmIsConnected(boolean mIsConnected) {
        this.mIsConnected = mIsConnected;
    }
}
