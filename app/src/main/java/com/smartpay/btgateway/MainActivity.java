package com.smartpay.btgateway;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Button;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import com.smartpay.btgateway.WCBluetoothProtocol.WCBluetoothProtocol;
import com.smartpay.btgateway.WCBluetoothProtocol.WCBtProtRequest;
import com.smartpay.btgateway.WCBluetoothProtocol.WCBtProtResponse;
import com.smartpay.btgateway.comm.WC_EthernetConnect;

public class MainActivity extends Activity {
    TextView myLabel;
    EditText myTextbox;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    int readBufferPosition;
    int counter;
    volatile boolean stopWorker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button openButton = (Button) findViewById(R.id.open);
        Button closeButton = (Button) findViewById(R.id.close);
        myLabel = (TextView) findViewById(R.id.label);
        //Open Button
        openButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    findBT();
                    openBT();
                } catch (IOException ex) {
                }
            }
        });


        //Close button
        closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    closeBT();
                } catch (IOException ex) {
                }
            }
        });
    }

    void findBT() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            myLabel.setText("No bluetooth adapter available");
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetooth, 0);
        }

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equals("MP200:FA02B9AB15")) {
                    mmDevice = device;
                    break;
                }
            }
        }
        myLabel.setText("Bluetooth Device Found");
    }

    void openBT() throws IOException {
        UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //Standard SerialPortService ID
        mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
        mmSocket.connect();
        mmOutputStream = mmSocket.getOutputStream();
        mmInputStream = mmSocket.getInputStream();

        beginListenForData();

        myLabel.setText("Bluetooth Opened");
    }

    void beginListenForData() {
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character

        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];

        workerThread = new Thread(new Runnable() {
            WCBluetoothProtocol bthProtocol = new WCBluetoothProtocol();
            WCBtProtRequest wcBtProtRequest;
            WCBtProtResponse wcBtProtResponse;
            WC_EthernetConnect wcEthernetConnect;

            public void run() {
                while (!Thread.currentThread().isInterrupted() && !stopWorker && mmInputStream != null) {
                    try {
                        int bytesAvailable = mmInputStream.available();
                        //Log.d("comm" , "bytes available ["+bytesAvailable+"]" );
                        if (bytesAvailable > 0) {
                            Log.d("main", "bytes available [" + bytesAvailable + "]");
                            byte[] packetBytes = new byte[bytesAvailable];
                            mmInputStream.read(packetBytes);
                            System.out.println(HexUtils.formatHexDump(packetBytes, 0, bytesAvailable));

                            //Build up response
                            wcBtProtRequest = new WCBtProtRequest(packetBytes, bytesAvailable);
                            wcBtProtResponse = bthProtocol.ProcessRequest(wcBtProtRequest);

                            if (wcBtProtResponse != null) {
                                boolean isCmdSuccess = false;
                                switch (wcBtProtResponse.byCommandType) {
                                    case WCBluetoothProtocol.WC_DO_OPEN_CMD:
                                        if (wcEthernetConnect != null) {
                                            wcEthernetConnect.disconnect();
                                        }
                                        wcEthernetConnect = new WC_EthernetConnect();
                                        isCmdSuccess = true;
                                        if (isCmdSuccess) {
                                            Log.d("main", "WC_DO_OPEN_CMD -> ACK");
                                            mmOutputStream.write(WCBluetoothProtocol.ACK);
                                        } else {
                                            Log.d("main", "WC_DO_OPEN_CMD -> NAK");
                                            mmOutputStream.write(WCBluetoothProtocol.NAK);
                                        }
                                        mmOutputStream.flush();
                                        break;
                                    case WCBluetoothProtocol.WC_DO_CONNECT_CMD:

                                        wcEthernetConnect.setmIpAddress(wcBtProtResponse.btProtConnection.szHostIP);
                                        wcEthernetConnect.setmPort(Integer.valueOf(wcBtProtResponse.btProtConnection.szHostPort));
                                        wcEthernetConnect.setiSendTimeout(wcBtProtResponse.btProtConnection.iSendTimeout);
                                        wcEthernetConnect.setiRecvTimeout(wcBtProtResponse.btProtConnection.iRecvTimeout);
                                        wcEthernetConnect.setiConnTimeout(wcBtProtResponse.btProtConnection.iConnTimeout);

                                        isCmdSuccess = true;
                                        if (wcEthernetConnect.init()) {
                                            if (wcEthernetConnect.connect()) {
                                                isCmdSuccess = true;
                                            }
                                        }

                                        if (isCmdSuccess) {
                                            Log.d("main", "WC_DO_CONNECT_CMD -> ACK");
                                            mmOutputStream.write(WCBluetoothProtocol.ACK);
                                        } else {
                                            Log.d("main", "WC_DO_CONNECT_CMD -> NAK");
                                            mmOutputStream.write(WCBluetoothProtocol.NAK);
                                        }
                                        mmOutputStream.flush();
                                        break;

                                    case WCBluetoothProtocol.WC_DO_SEND_ONLY_CMD:
                                        if (wcEthernetConnect != null) {
                                            if (!wcEthernetConnect.ismIsConnected()) {
                                                wcEthernetConnect.connect();
                                            }
                                            if (wcEthernetConnect.ismIsConnected()) {

                                                isCmdSuccess = wcEthernetConnect.send(wcBtProtResponse.dataBuffer, wcBtProtResponse.iDataBufferLen);
                                                if (isCmdSuccess) {

                                                    Log.d("main", "WC_DO_SEND_ONLY_CMD -> ACK");
                                                    mmOutputStream.write(WCBluetoothProtocol.ACK);
                                                    mmOutputStream.flush();
                                                }
                                            }
                                        }else {
                                            mmOutputStream.write(WCBluetoothProtocol.EOT);
                                            mmOutputStream.flush();
                                        }
                                        break;
                                    case WCBluetoothProtocol.WC_DO_RECV_ONLY_CMD:
                                        Log.d("main", "WC_DO_RECV_ONLY_CMD -> ACK");
                                        mmOutputStream.write(WCBluetoothProtocol.ACK);
                                        mmOutputStream.flush();

                                        wcBtProtResponse.dataBuffer = new byte[WCBluetoothProtocol.WC_MSG_DATA_MAX_LEN];
                                        wcBtProtResponse.iDataBufferLen = wcEthernetConnect.receive(wcBtProtResponse.dataBuffer, wcBtProtResponse.iDataBufferLen);
                                        if (wcBtProtResponse.iDataBufferLen > 0) {
                                            //Pack response data
                                            if (bthProtocol.PackResponse(wcBtProtResponse)) {

                                                Log.d("main", "Sending back data ");
                                                Log.d("main", HexUtils.formatHexDump(wcBtProtResponse.dataBuffer, 0, wcBtProtResponse.iDataBufferLen));
                                                mmOutputStream.write(wcBtProtResponse.dataBuffer, 0, wcBtProtResponse.iDataBufferLen);
                                                mmOutputStream.flush();

                                                int iMaxTimeout = 300;
                                                int iTimeout = 0;
                                                while ((bytesAvailable = mmInputStream.available()) == 0 && iTimeout < iMaxTimeout) {
                                                    Thread.sleep(5);
                                                    if (bytesAvailable > 0) {
                                                        packetBytes = new byte[bytesAvailable];
                                                        mmInputStream.read(packetBytes);
                                                        if (packetBytes[0] == WCBluetoothProtocol.ACK) {
                                                            Log.d("main", "ACK received");
                                                            break;
                                                        } else {
                                                            Log.d("main", "WRONG Data received");
                                                            Log.d("main", HexUtils.formatHexDump(packetBytes, 0, packetBytes.length));
                                                            break;
                                                        }
                                                    }
                                                    iTimeout++;
                                                }
                                            }
                                        }
                                        break;
                                    case WCBluetoothProtocol.WC_DO_SEND_RECV_P_CMD:
                                    case WCBluetoothProtocol.WC_DO_SEND_RECV_U_CMD:
                                        if (wcEthernetConnect != null) {
                                            if (!wcEthernetConnect.ismIsConnected()) {
                                                wcEthernetConnect.connect();
                                            }
                                            if (wcEthernetConnect.ismIsConnected()) {

                                                isCmdSuccess = wcEthernetConnect.send(wcBtProtResponse.dataBuffer, wcBtProtResponse.iDataBufferLen);
                                                if (isCmdSuccess) {

                                                    Log.d("main", "WC_DO_SEND_RECV_P_CMD/WC_DO_SEND_RECV_U_CMD -> ACK");
                                                    mmOutputStream.write(WCBluetoothProtocol.ACK);
                                                    mmOutputStream.flush();

                                                    wcBtProtResponse.dataBuffer = new byte[WCBluetoothProtocol.WC_MSG_DATA_MAX_LEN];
                                                    wcBtProtResponse.iDataBufferLen = wcEthernetConnect.receive(wcBtProtResponse.dataBuffer, wcBtProtResponse.dataBuffer.length);
                                                    if (wcBtProtResponse.iDataBufferLen > 0) {
                                                        //Pack response data
                                                        if (bthProtocol.PackResponse(wcBtProtResponse)) {

                                                            Log.d("main", "Sending back data ");
                                                            Log.d("main", HexUtils.formatHexDump(wcBtProtResponse.dataBuffer, 0, wcBtProtResponse.iDataBufferLen));
                                                            mmOutputStream.write(wcBtProtResponse.dataBuffer, 0, wcBtProtResponse.iDataBufferLen);
                                                            mmOutputStream.flush();

                                                            int iMaxTimeout = 30;
                                                            int iTimeout = 0;
                                                            while ((bytesAvailable = mmInputStream.available()) == 0 && iTimeout < iMaxTimeout) {
                                                                Thread.sleep(50);
                                                                Log.d("main", "Waiting for ACK ");
                                                                if (bytesAvailable > 0) {
                                                                    packetBytes = new byte[bytesAvailable];
                                                                    mmInputStream.read(packetBytes);
                                                                    if (packetBytes[0] == WCBluetoothProtocol.ACK) {
                                                                        Log.d("main", "ACK received");
                                                                        break;
                                                                    } else {
                                                                        Log.d("main", "WRONG Data received");
                                                                        Log.d("main", HexUtils.formatHexDump(packetBytes, 0, packetBytes.length));
                                                                        break;
                                                                    }
                                                                }
                                                                iTimeout++;
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    Log.d("main", "WC_DO_SEND_RECV_P_CMD/WC_DO_SEND_RECV_U_CMD -> EOT");
                                                    mmOutputStream.write(WCBluetoothProtocol.EOT);
                                                    mmOutputStream.flush();
                                                }
                                            }
                                        }
                                        break;
                                    case WCBluetoothProtocol.WC_DO_DISCONNECT:
                                        isCmdSuccess = wcEthernetConnect.disconnect();
                                        if (isCmdSuccess) {
                                            Log.d("main", "WC_DO_DISCONNECT -> ACK");
                                            mmOutputStream.write(WCBluetoothProtocol.ACK);
                                        } else {
                                            Log.d("main", "WC_DO_DISCONNECT -> NAK");
                                            mmOutputStream.write(WCBluetoothProtocol.NAK);
                                        }
                                        mmOutputStream.flush();
                                        break;
                                    case WCBluetoothProtocol.WC_DO_CLOSE_CMD:
                                        isCmdSuccess = true;
                                        if (isCmdSuccess) {
                                            Log.d("main", "WC_DO_CLOSE_CMD -> ACK");
                                            mmOutputStream.write(WCBluetoothProtocol.ACK);
                                        } else {
                                            Log.d("main", "WC_DO_CLOSE_CMD -> NAK");
                                            mmOutputStream.write(WCBluetoothProtocol.NAK);
                                        }
                                        mmOutputStream.flush();
                                        break;
                                }


                            }
                        }
                    } catch (IOException ex) {
                        stopWorker = true;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        workerThread.start();
    }

    void sendData() throws IOException {
        String msg = myTextbox.getText().toString();
        msg += "\n";
        mmOutputStream.write(msg.getBytes());
        myLabel.setText("Data Sent");
    }

    void closeBT() throws IOException {
        stopWorker = true;
        mmOutputStream.close();
        mmInputStream.close();
        mmSocket.close();
        myLabel.setText("Bluetooth Closed");
    }
}