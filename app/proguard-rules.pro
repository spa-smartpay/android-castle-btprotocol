# This is a configuration file for ProGuard.
# http://proguard.sourceforge.net/index.html#manual/usage.html
#
# This file is no longer maintained and is not used by new (2.2+) versions of the
# Android plugin for Gradle. Instead, the Android plugin for Gradle generates the
# default rules at build time and stores them in the build directory.
 
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose

 -ignorewarnings

# Optimization is turned off by default. Dex does not like code run
# through the ProGuard optimize and preverify steps (and performs some
# of these optimizations on its own).
-dontoptimize
-dontpreverify
# Note that if you want to enable optimization, you cannot just
# include optimization flags in your own project configuration file;
# instead you will need to point to the
# "proguard-android-optimize.txt" file instead of this one from your
# project.properties file.
 
-keepattributes *Annotation*
-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService
 
 
# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
    native <methods>;
}
 
# keep setters in Views so that animations can still work.
# see http://proguard.sourceforge.net/manual/examples.html#beans
-keepclassmembers public class * extends android.view.View {
   void set*(***);
   *** get*();
}
 
# We want to keep methods in Activity that could be used in the XML attribute onClick
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}
 
# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
 
-keepclassmembers class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator CREATOR;
}
 
-keepclassmembers class **.R$* {
    public static <fields>;
}
 
-keep class **.R$*
 
# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}
 
# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**
 
# Understand the @Keep support annotation.
-keep class android.support.annotation.Keep
 
-keep @android.support.annotation.Keep class * {*;}
 
-keepclasseswithmembers class * {
    @android.support.annotation.Keep <methods>;
}
 
-keepclasseswithmembers class * {
    @android.support.annotation.Keep <fields>;
}
 
-keepclasseswithmembers class * {
    @android.support.annotation.Keep <init>(...);
}
 
##---------------Begin: proguard configuration for Gson  ----------
-keepattributes *Annotation*
# -keepclassmembers class com.android.application.** { *; }
# -keepclassmembers class ro.cloudandsun.** { *; }
-keepclassmembers class ro.cloudandsun.paymentapplication.ModelTerminal { *; }
-keepclassmembers class ro.cloudandsun.paymentapplication.SHCLOG { *; }
-keepclassmembers class ro.cloudandsun.paymentapplication.ModelTransaction { *; }
-keepclassmembers class ro.cloudandsun.paymentapplication.ModelUser { *; }
-keepclassmembers class ro.cloudandsun.paymentapplication.SettingsModel { *; }
 
-keepclassmembers class ro.cloudandsun.paymentapplication.TLSSocketFactory { *; }
-keepclassmembers class ro.cloudandsun.paymentapplication.ContinuousTcpClient { *; }
-keepclassmembers class ro.cloudandsun.paymentapplication.ContinuousTcpServiceImp { *; }
 
 
-keepattributes InnerClasses
 
-keepattributes Signature
 
# Gson specific classes
-dontwarn sun.misc.**
#-keep class com.google.gson.stream.** { *; }
 
# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }
 
# Prevent proguard from stripping interface information from TypeAdapterFactory,
# JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer
 
##---------------End: proguard configuration for Gson  ----------
 